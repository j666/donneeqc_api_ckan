

import csv
from services.serviceTxt_new import *
from services.serviceUtil import *

class ServiceCSV:
    def __init__(self, fileCsv, delimiter=","):

        self.fileCsv= fileCsv
        self.delimiter = delimiter
        self.servUtil = ServicesUtils()
        self.servTxt = None

        # self.colList = self.get_colonne()
        # self.nbCol = len(self.colList)


    def setFileCsv(self, fileCsv):
        self.fileCsv = fileCsv


    def check_carac_interdit(self, aCheck, list= True):
        CARAC_INTERDIT = ['é','É', 'è', 'È', 'à', 'À', 'ê', 'Ê', 'ë','Ë', 'û', 'Û', 'ç', 'ù', 'Ù', '&', '/', '=', '?', ' ']
        if list:
            for i in aCheck:
                for carac in CARAC_INTERDIT:
                    if carac in i:
                        raise Exception("Des caracteres interdits sont présent: '{0}'".format(carac))
                        # raise Exception("Des caracteres interdits sont présent: '{0}'. \nList des caracteres interdit= {1}".format(carac, CARAC_INTERDIT))


    def getListOfListToWrite(self, inputToWrite, listColumn, inputType='listOfDict'):
        """ Méthode qui renvoie une list de list des données passé en parametre dans listeColumn
        PARAM: inputToWrite: listDict
            listColumn: list de string des nom de colonne-attribut

        Exemple apel:
            colToWrite = [ 'name', 'id', 'metadata_created']
            listOfListToWrite = servCSV.getListOfListToWrite(inputToWrite=listDictCreateUntilDate, listColumn=colToWrite, inputType='listOfDict')
        """

        listDonneeAEcrire = []

        listDonneeAEcrire.append(listColumn)

        if inputType == 'listOfDict':
            for aDict in inputToWrite:
                aListToWrite = []
                for att in listColumn:
                    aListToWrite.append(aDict[att])
                listDonneeAEcrire.append(aListToWrite)
        else:
            raise Exception('Il faut implementer cette partie ou input est un dictOfDict')
        # for aPack in packDict.values():
        #     for resource in aPack['resources']:
        #         if resource['url'][0:3] == 'ftp' or resource['url'][0:3] == 'FTP':
        #             # listInfoEcrire =
        #             listDonneeAEcrire.append([aPack['name'], aPack['organization']['title'], aPack['author_email'], resource['id'], resource['name'], resource['url']])

        return listDonneeAEcrire

    # def check_delimiter(self, delimiter=","):

        # mal fonction leve exception avec certain fichier qui ne sont pas des problèeme réel
        # Incorporé plutot dans le get_colonne...
        # id: 8db37749-6e61-472f-8da4-574c459e2a6f

        # with open(self.fileCsv, newline = "", encoding="utf-8") as csvfile:
        # # with open(self.fileCsv, newline = "", encoding="utf-8-sig") as csvfile:
        #     try:
        #         dialect = csv.Sniffer().sniff(csvfile.read(1024), delimiters = delimiter)
        #         # print("Delimiter is OK")
        #     except Exception as e:
        #         if type(e) is UnicodeDecodeError:
        #             raise Exception("Probleme encodage n'est pas UTF-8")
        #         else:
        #             raise Exception("le delimiter du CSV n'est pas conforme à ce qui est attendu : {}".format(delimiter))


    def get_colonne(self):

        try:
            with open(self.fileCsv, newline="", encoding="utf-8-sig") as csvfile:
                reader = csv.reader(csvfile, delimiter=self.delimiter)
                for row in reader:
                    try:
                        self.check_carac_interdit(row)
                    except Exception as e:
                        raise Exception("Problème détecté dans la première ligne du CSV contenant les noms de colonnes: {}".format(e.__str__()))
                    if len(row)== 0:
                        raise Exception("Problème la première ligne du CSV est vide")
                    if len(row) == 1:
                        readerPointVirgule = csv.reader(csvfile, delimiter=";")
                        for rowReader2 in readerPointVirgule:
                            if len(rowReader2) > 1:
                                raise Exception("Le délimiteur utilisé est incorect, la virgule(,) doit être utilisée comme délimiteur CSV et non le ; \nEntête du fichier={}".format(row))
                            break
                        readerBarre = csv.reader(csvfile, delimiter="|")
                        for rowBarre in readerBarre:
                            if len(rowBarre) > 1:
                                raise Exception("Le délimiteur utilisé est incorect, la virgule(,) doit être utilisée comme délimiteur CSV et non | \nEntête du fichier={}".format(row))
                            elif len(rowReader2) == 1:
                                raise Exception("À vérifier, la première ligne du CSV contient une seule colonne, peut être un problème avec les doubles guillemet mals utilisés : {}".format(row))
                            break
                    return row
                # print('ici')

        except Exception as e:
            if type(e) is FileNotFoundError:
                raise Exception("Fichier introuvable, vérifier le chemin d'accès et le nom du fichier saisi: {}".format(self.fileCsv))
            elif type(e) is UnicodeDecodeError:
                raise Exception("Probleme encodage n'est pas UTF-8")
            else:
                raise




    def check_allRowFitNbCol(self):

        listRecProbleme = []
        try:
            with open(self.fileCsv, newline="", encoding="utf-8-sig") as csvfile: ## sig = sans caractere BOM de debut
                reader = csv.reader(csvfile, delimiter=self.delimiter)
                nbLigne = 0
                for row in reader:
                    if nbLigne == 212:
                        print('ici')
                    # print(row)
                    nbLigne += 1
                    if len(row) != self.nbCol:
                        listRecProbleme.append(nbLigne)
                    # print('ok')
        except Exception as e:
            # Error('field larger than field limit (131072)')
            if type(e) is UnicodeDecodeError:
                raise Exception("Probleme encodage n'est pas UTF-8")
            else:
                print('ERROR line {}'.format(nbLigne), e)


        if len(listRecProbleme) >0:
            raise Exception("Problèmes dans le nombre de colonne. Peut être  les doubles guillemets mal utilisés.\nVoir no. ligne dans CSV:{}".format(listRecProbleme))
            # raise Exception("Problèmes dans les données probablement le delimiteur et les doubles guillemets mal utilisés voir no. ligne dans CSV:{}".format(listRecProbleme))
        # else:
            # print('OK verif guillemet structure')


    def validePlusieurCsv(self, repertoireContenantLesCSV):
        """ Méthode qui valide tout les CSV contenue dans un repertoire sur ordinateur local et retourne un rapport en txt avec les problèmes rencontrés 
        dans chaque csv 
        """
        self.servTxt = ServiceTxtNew('{}erreurValidationCsv.txt'.format(repertoireContenantLesCSV))
        self.servTxt.creer_fichier_ecrire_entete('Validation plusieurs CSV locaux du repertoire: {}\n'.format(repertoireContenantLesCSV))

        listCsvAValid = self.servUtil.getListPathAllFileInDir(repertoireContenantLesCSV)
        for unFichier in listCsvAValid:

            if unFichier[-3:] != 'csv':
                continue

            self.setFileCsv(unFichier)
            self.servTxt.ecrire("Validation du CSV: {}".format(unFichier))
            # try:
            #     self.colList = self.get_colonne()
            #     self.nbCol = len(self.colList)
            #     self.check_delimiter()
            #     self.check_allRowFitNbCol()
            #     self.servTxt.ecrire("CSV valide sans erreur!!")

            # except Exception as e:
            #     self.servTxt.ecrire(e.__str__())
            #     print('problème fichier: {}'.format(unFichier))
            try:
                self.valideCsv()
                self.servTxt.ecrire("CSV valide sans erreur!!")

            except Exception as e:
                messErr = 'problème fichier: {}'.format(unFichier)
                self.servTxt.ecrire(messErr)


            self.servTxt.ecrire("\n")

        print('validation terminé voir rapport')

    def valideCsv(self):
        """ Méthode qui valide le CSV en attribut de l'object ServiceCSV   
            self.fileCsv
        """
        # if self.servTxt is None:
        #     self.servTxt = ServiceTxtNew(pathTxtErreur)
        #     self.servTxt.creer_fichier_ecrire_entete('Validation CSV: \n{}'.format(self.fileCsv))

        try:
            self.colList = self.get_colonne()
            self.nbCol = len(self.colList)
            # self.check_delimiter()
            self.check_allRowFitNbCol()

        except Exception as e:
            # self.servTxt.ecrire('Validation CSV: \n{}'.format(self.fileCsv))
            # self.servTxt.ecrire("\n")
            # self.servTxt.ecrire(e.__str__())
            # self.servTxt.ecrire("\n")
            raise

        print('validation terminé, le CSV est ok ✅')

    def ecrireCSVFromListOfList(self, listOfList):

        # On remplace les retours à la ligne dans toutes les chaînes de données
        # listOfListSansRetour = [[element.replace("\n", " ").replace("\r", " ") for element in ligne] for ligne in listOfList]
        self.enleverRetourALaLigneInListofList(listOfList)
        
        with open(self.fileCsv, "w", newline="",encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerows(listOfList)

    def enleverRetourALaLigneInListofList(self, listOfList):
        newListOfList = []
        idxList = -1
        for uneList in listOfList:
            idxList += 1
            inxElem = -1
            for elem in uneList:
                inxElem +=1
                if elem == '548203976727911131949666':
                    print('ici')
                
                if isinstance(elem, str):
                    elem = elem.replace("\r", "")
                    elem = elem.replace("\n", "")
                    uneList[inxElem] = elem
            listOfList[idxList] = uneList
            
        print ('ici')

        

if __name__ == '__main__':


    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/cheminPasRap.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/coop-EncodeUtf8.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_encodeainsi.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_donneeGuillemetMalForme.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_accentTitreColonne.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_espTitreColonne.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_delimiteur.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/casErreur/err_premiereLigneVide.csv")
    # servCsv = ServiceCSV("C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/probDonneeGuillemet_extract_notice_scp_20230423.csv")
    # servCsv = ServiceCSV("C:/Users/Propriétaire/Downloads/coop-non-financieres-actives_20211201.csv")


    # servCsv.valideCsv()




################
    #list de file

    #
    path= "C:/temp/aValide/"
    # path= "C:/Users/Propriétaire/Downloads/"
    listAValid = [
        # "{}rf-2022-absent.csv".format(path),
        "{}extract_notice_scp_20230521.csv".format(path),
        "{}ARR.csv".format(path),
        "{}Liste_UA.csv".format(path),
        "{}Liste_UG.csv".format(path),
        "{}MRC.csv".format(path),
        "{}odonymes_officiels.csv".format(path),
        "{}toponymes_officiels.csv".format(path),
        "{}MUN.csv".format(path),
        # "{}rf-2022-mrc.csv".format(path),
        # "{}rf-2022-regie.csv".format(path),
        # "{}rf-2022-mun.csv".format(path)

    ]
    listExcept = []
    for i in listAValid:
        print(i)
        try:
            servCsv = ServiceCSV(i)
            servCsv.valideCsv()

        except Exception as e:
            print('en probleme', e)
            listExcept.append(e)





