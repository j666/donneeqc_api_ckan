
import subprocess
from subprocess import CalledProcessError
import os

class ServicesUtils:
    def __init__(self):
        self.directory = None

    # def setDir(self, pathDirectory):
    #     self.directory = pathDirectory

    def getDateHeure_actuelle(self, heure=True):
        from datetime import datetime
        """ Fonction qui retourne date et heure actuelle:
        Exemple de retour: 2016-10-28T11h50min22sec ou 20230509

        """

        date_time = datetime.now()
        if heure:
            date_format = date_time.strftime("%Y-%m-%dT%Hh%Mmin%Ssec")
        else:
            date_format = date_time.strftime("%Y%m%d")
        return date_format

    def getDatePastNBDay(self, nbDayAgo):

        import datetime

        today = datetime.date.today()
        # weekday = today.weekday()
        day7dayAgo= today - datetime.timedelta(nbDayAgo)
        # dateString = "{0}-{1}-{2}".format(day7dayAgo.strftime("%Y"), day7dayAgo.strftime("%m"),day7dayAgo.strftime("%d"))  # convert in string
        return day7dayAgo

    def convertStringToDate(self, stringToConvert):
        # stringToConvert = '2023-06-06'
        import datetime
        if isinstance(stringToConvert, datetime.date):
            return
        date = datetime.datetime(int(stringToConvert[0:4]), int(stringToConvert[5:7]), int(stringToConvert[8:10])).date()
        # date = datetime.datetime.strptime(stringToConvert, "%y-%m-%d")
        return date

    def getListPathAllFileInDir(self, path):

        listFile = []
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:

                listFile.append(os.path.join(root, name))
                # print(os.path.join(root, name))

            # for name in dirs:
                # print(os.path.join(root, name))

        return listFile


    def CMD_execute(self, cmd):

        try:
            if isinstance(cmd, str):
                cmdByte = cmd
            else:
                cmdByte = cmd.encode('850')  # on convertie unicode en STR. python2

            outputBytes = subprocess.check_output(cmdByte, stdin=None, stderr=subprocess.STDOUT,
                                         shell=True)
            outputStr = str(outputBytes)

            if outputStr[:5] == "ERROR" or "FAILURE" in outputStr:
                print('probleme verif...')
                raise

        except Exception as e:

            print('erreur dans lancement cmd')
            if isinstance(e, CalledProcessError):
                print('dedans call process excep')

            raise



if __name__ == '__main__':
    objSer = ServicesUtils()

    date = objSer.getDatePastNBDay(7)
    # objSer.setDir("F:/LidarMAPAQformatWebZip")
    # listPathFile = objSer.getListPathAllFileInDir()
    #
    # fileTest = "C:/temp/test/test2.txt"
    #
    # cmd= "aws s3api put-object --bucket arn:aws:s3:ca-central-1:993682315089:accesspoint/ap-dq-prd-mapaq --key mapaq/test3.txt --body {}".format(fileTest)
    #
    # try:
    #     objSer.CMD_execute(cmd)
    # except:
    #     print('probleme')
    #
    # print('finit')





