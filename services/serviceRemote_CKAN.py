
from ckanapi import RemoteCKAN
from JETONSJOSEE import JETONSJOSEE


class ErreurApiDQ(Exception):
    """ Josee Martel 2023-04
        Classe exception lance lors d'un probleme.
        Exemple apel:   try:
                            ...
                            raise ErreurApiDQ

                        except ErreurApiDQ as e:
                            print (e.message)
    """

    def __init__(self, message=u'Probleme Api Donnee Quebec', type=u'ErreurAPIDQ'):
        self.message = message
        self.type = type

    def __str__(self):
        return repr(self.message)


class ServiceCkanApiDQ:
    """ class qui communique avec API CKAN de Donnee Quebec via package RemoteCKAN

    Exemple utilisation:
  ...

    """

    def __init__(self, token='', enTest=False):

        if enTest:
            self.urlAdress = "https://beta.donneesquebec.ca/"
        else:
            self.urlAdress = "https://www.donneesquebec.ca/"

        # self.urlAction = "https://www.donneesquebec.ca/recherche/api/3/action/"
        # self.urlPatch = "https://www.donneesquebec.ca/recherche/api/3/action/resource_patch"  # modif valeur

        self.token = token
        self.servCkanApi = RemoteCKAN(address=self.urlAdress, apikey=self.token)
        # self.postHeader = {
        #     "Authorization": self.apiKey,
        #     "Connection": "keep-alive",
        #     "Upgrade-Insecure-Requests": "1",
        #     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
        #     "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        #     "Sec-Fetch-Site": "same-origin",
        #     "Sec-Fetch-Mode": "navigate",
        #     "Sec-Fetch-User": "?1",
        #     "Sec-Fetch-Dest": "document",
        #     "Referer": "https://www.google.com/",
        #     "Accept-Encoding": "gzip, deflate, br",
        #     "Accept-Language": "en-US,en;q=0.9"
        # }

    # def __del__(self):
    # del self.ogr
    # del self.serviceBd


if __name__ == '__main__':

    # jeton = JETONSJOSEE['PROD']
    jeton = JETONSJOSEE['BETA']

    action = "recherche/api/3/action/organization_list?"
    servApi = ServiceCkanApiDQ(token=jeton, enTest=True)
    # lesOrg = servApi.servCkanApi.action.organization_list()
    lesOrg = servApi.servCkanApi.action.package_search()

    print('ici')



# ### organisation
#
# org = betaDq.action.organization_list()
# detailBanq = betaDq.action.organization_show(id='banq')
#
#
# ### datastore
# id_ressource_municipaite="19385b4e-5503-4330-9e59-f998f5918363"
# dictDonnee = betaDq.action.datastore_info(id=id_ressource_municipaite)
#
#
# packList = betaDq.action.package_list()
#
# print('ici')

