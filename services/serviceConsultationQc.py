import json
import sys
# pour fonctionnement dans VS code, on doit ajouter le path pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")

from services.serviceCsv import *
import copy

class ServiceConsultationQc:

    """ class qui utilise le JSON exporte de consultation Québec


    """


    def __init__(self, jsonFile):

        self.jsonFile = jsonFile
        self.data = None
        self.newData = []

        self.DICTQUESTION = {
                1: "1. Comment avez-vous découvert l’existence du portail Données Québec?",
                2: '2. À quelle fréquence visitez-vous le portail Données Québec?',
                3: '3. Pour quelles raisons visitez-vous Données Québec?',
                4: '4. Lors de votre dernière visite, avez-vous trouvé ce que vous cherchiez?',
                5: '5. Combien de jeux de données différents avez-vous téléchargés à partir de Données Québec jusqu’à ce jour (excluant les téléchargements faits après une mise à jour du jeu)?',
                6: '6. Quel usage avez-vous fait des jeux de données que vous avez téléchargés jusqu’à aujourd’hui? ',
                7: '7. Connaissez-vous une utilisation (application logicielle, article dans un quotidien, recherche universitaire, etc.) de données ouvertes au Québec? Si oui, vous êtes invité à partager les informations en ce qui concerne ces utilisations.',
                8:'8. Quels thèmes devrait-on prioriser pour offrir de nouveaux jeux de données ouvertes?',
                9: '9. Quel est votre degré de satisfaction en ce qui concerne les éléments suivants?',
                10: '10. Avez-vous déjà participé à au moins une compétition de développement d’application (hackathon)?',
                11: '11. À quel titre utilisez-vous le portail Données Québec?',
                12: '12. Dans quelle catégorie d’âge vous situez-vous?',
                13: '13. Dans quelle région habitez-vous?'
            }

    def convertListDict2ListList(self, listDict):
        newlistOfList = []
        listEntete = []
        for key, val in listDict[0].items():
            if key == self.DICTQUESTION[9]:
                continue
            listEntete.append(key)

        newlistOfList.append(listEntete)

        for aDict in listDict:
            alist = []
            for key, val in aDict.items():
                if key == self.DICTQUESTION[9]:
                    continue
                alist.append(val)

            newlistOfList.append(alist)
        return newlistOfList


    def loadJson(self):
        with open(self.jsonFile, encoding='utf-8') as fp:
            self.data = json.load(fp)
        # print('ici')

    def jsonConserveUneAnne(self, anneeAConserve):

        # for aRep in self.data:
        self.data = [d for d in self.data if anneeAConserve in d['Répondu le']] #condition on garde

            # print('ici')
            # if anneeAConserve in aRep['Répondu le']:
            #     print('a conserve')




    def eclateReponseQuestionChoixMultiple(self, noQuestion):
        """ retour new dict avec des enregistrements dédoublé dans le cas de reponse multiple"""
        newData = []
        questionID = self.DICTQUESTION[noQuestion]

        dataDeepCopy = copy.deepcopy(self.data)
        for uneRepDict in dataDeepCopy:
        # for uneRepDict in self.data:


            listChoixMult = uneRepDict[questionID]

            for unChoix in listChoixMult:
                newDictRep = uneRepDict
                newDictRep[questionID] = unChoix
                newData.append(copy.deepcopy(newDictRep))
        return newData

    def aplatirListReponse(self, data, questionExclure):
        """converti en string les lists de reponse"""
        questionAAplatir = [1, 2, 3, 4, 5, 6, 10, 11, 12, 13]  ## todo 9, list de list...
        if questionExclure:
            questionAAplatir.remove(questionExclure)
            
        newDataPlate2 = []
        for uneReponseDict in data:
        # for index in range(len(data)):
        #     print(index)
        #     uneReponseDict = data[index]
            for noQ in questionAAplatir:
                idQues = self.DICTQUESTION[noQ]
                listChoixReponse = uneReponseDict[idQues]
                stringRep = ''
                for rep in listChoixReponse:
                    stringRep += rep +","
                stringRep = stringRep[:-1]

                uneReponseDict[idQues] = stringRep
                # uneReponseDict[idQues] = ','.join(choixReponse)
            newDataPlate2.append(copy.deepcopy(uneReponseDict))

        return newDataPlate2



if __name__ == '__main__':


    jsonPath = "C:/Users/marjo31/local_job/temp/2024-53267.json"
    # jsonPath = "C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/consultationQc/20230720.json"
    servConsultation = ServiceConsultationQc(jsonPath)

    servConsultation.loadJson()

    servConsultation.jsonConserveUneAnne()

    newData = servConsultation.eclateReponseQuestionChoixMultiple(3)
    # newData = servConsultation.eclateReponseQuestionChoixMultiple(8)
    test = copy.deepcopy(newData)
    # test[1]["1. Comment avez-vous découvert l’existence du portail Données Québec?"] = 'fuck '
    newDataPlatenew = servConsultation.aplatirListReponse(test)
    listOfList = servConsultation.convertListDict2ListList(newDataPlatenew)

    pathCsvSortie = "C:/Users/marjo31/local_job/temp/cq_exportQuestion3Eclate.csv"
    servCsv = ServiceCSV(pathCsvSortie)
    servCsv.ecrireCSVFromListOfList(listOfList)

    print('ici')

