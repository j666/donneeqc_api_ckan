
# import  urllib, requests
import urllib3, urllib, requests
import json
# import datetime
from datetime import datetime
import json
import sys
# pour fonctionnement dans VS code, on doit ajouter le path pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")

import re
from services.serviceUtil import *

class ErreurApiDQ(Exception):
    """ Josee Martel 2023-04
        Classe exception lance lors d'un probleme.
        Exemple apel:   try:
                            ...
                            raise ErreurApiDQ
                        
                        except ErreurApiDQ as e:
                            print (e.message)
    """
    def __init__(self, message=u'Probleme Api Donnee Quebec', type=u'ErreurAPIDQ'):
        self.message = message
        self.type = type

    def __str__(self):
        return repr(self.message)

class ServiceAPI_DQ:

    """ class qui communique avec API CKAN de Donnees Quebec via package urllib3

    Exemple utilisation:

    # 1-construction objet ServiceAPI:

        objServiceDQ = ServiceAPI_DQ()

    # 2-lancement differentes methodes

        dateEDT = objService.convertDateUTCToEDT('2021-06-21 16:22:22.222222')
        dateUTC = objService.convertDateEDTToUTC('2021-06-28')
        orgId = objService.getOrganizationId('mffp')
        listDictPackageForOrganization = objService.getPackageListDictForAOrganization('mffp')
        objService.writeCSVResourceFromDict(listDictPackageForOrganization, 'c:/temp', 'utf-8', 'last_modified')
        objService.writeCSVInfoResourceForOrganisation('mrn', "c:/temp/leCSV.csv", 'utf-8'', 'last_modified')

        ...

    """


    def __init__(self, apiKey='', enTest=False, environnement='DQ'):

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self.http = urllib3.PoolManager()
        self.urlAction = "https://www.donneesquebec.ca/recherche/api/3/action/"
        self.urlPatch = "https://www.donneesquebec.ca/recherche/api/3/action/resource_patch" # modif valeur
        self.idOrganisation = ''
        self.apiKey = apiKey
        self.servUtil = ServicesUtils()
        self.postHeader = {
            "Authorization":self.apiKey,
            'Content-Type':'application/x-www-form-urlencoded',
            "Connection": "keep-alive"
            # "Upgrade-Insecure-Requests": "1",
            # "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
            # "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            # "Sec-Fetch-Site": "same-origin",
            # "Sec-Fetch-Mode": "navigate",
            # "Sec-Fetch-User": "?1",
            # "Sec-Fetch-Dest": "document",
            # "Referer": "https://www.google.com/",
            # "Accept-Encoding": "gzip, deflate, br",
            # "Accept-Language": "en-US,en;q=0.9"
        }
        if enTest:
            self.urlAction = "https://beta.donneesquebec.ca/recherche/api/3/action/"
            self.urlPatch = "https://beta.donneesquebec.ca/recherche/api/3/action/resource_patch"
            self.urlPackPatch = "https://beta.donneesquebec.ca/recherche/api/3/action/package_patch" 

        if environnement == 'MTL':
            self.urlAction = "https://donnees.montreal.ca/api/3/action/"
    # def __del__(self):
        # del self.ogr
        # del self.serviceBd

    def getDateHeure_actuelle(self, heure=True):
        """ Fonction qui retourne date et heure actuelle:
        Exemple de retour: 2016-10-28T11h50min22sec
                            ou 20230509 avec param heure=False

        """

        date_time = datetime.now()
        if heure:
            date_format = date_time.strftime("%Y-%m-%dT%Hh%Mmin%Ssec")
        else:
            date_format = date_time.strftime("%Y%m%d")
        return date_format

    def convertDateUTCToEDT(self, date):

        UTC_OFFSET_TIMEDELTA = datetime.utcnow() - datetime.now()
        try:
            a = datetime.fromisoformat(date) - UTC_OFFSET_TIMEDELTA
        except Exception as e:
            raise ErreurApiDQ("Probleme avec la date: {} n'est pas au format standard AAAA-MM-JJTHH:MM:SS.SS".format(date))

        dateEDT = a.strftime("%Y-%m-%dT%H:%M:%S.%f")
        return dateEDT

    def convertDateEDTToUTC(self, date):
        """ Methode qui convertie la date EDT ou EST en date UTC
        NB: S'il n'y a pas d'heure d'indiquée, le système inscrira l'heure actuelle
        l’Heure Avancée De L’Est = EDT
        S'ajuste si heure avancée ou non EDT ou EST.
        """
        dateNow = datetime.now()

        try:
            dateISO = datetime.fromisoformat(date)
        except:
            raise ErreurApiDQ("Probleme rencontré! La date: {} n'est pas au format standard AAAA-MM-JJ HH:MM:SS.SS".format(date))

        if dateISO.hour == 0:
            dateISO = dateISO.replace(hour=dateNow.hour, minute=dateNow.minute, second=dateNow.second)

        UTC_OFFSET_TIMEDELTA = datetime.utcnow() - dateNow
        ## conversion date ET -> UTC en ajoutant la différence de temps trouvé entre UTC et ET
        a = dateISO + UTC_OFFSET_TIMEDELTA
        dateUTC = a.strftime("%Y-%m-%dT%H:%M:%S.%f")
        return dateUTC

    def convertListDict2DictDict(self, listDict):
        newDict = {}
        for unDict in listDict:
            newDict[unDict['id']] = unDict

        return newDict

    def convertDictDict2ListDict(self, dictDict):
        newlistOfDict = []
        for key, val in dictDict.items():
            newlistOfDict.append(val)
        return newlistOfDict

    def MAJ_postRequest(self, url, dictMaj):
        """    Méthode pour lancer des requetes POST de MAJ ou creation
            implemente les apels avec package Request et non urllib -> voir ici https://stackoverflow.com/questions/2018026/what-are-the-differences-between-the-urllib-urllib2-urllib3-and-requests-modul
        """
        try:
            dictMajFormat = urllib.parse.quote(json.dumps(dictMaj))
            response = requests.post(url, data=dictMajFormat, headers=self.postHeader, verify=False)

        except Exception as e:
            print('oh probleme dans la requete POST', e)
            raise

        if response.status_code != 200:
            
            responseDict = json.loads(response.text) # remet encoding ok!!!!! 
            try:
                messErreurReponse = responseDict['error']['message']
            except:
                messErreurReponse = responseDict['error']['name']

            if response.status_code == 403:
                messErreurRecu = "{0} - {1} : Problème accès! L'utilisateur à les accès en édition?. Le vpn est branché? \n{2}".format(response.reason, response.status_code, messErreurReponse)
                raise ErreurApiDQ("Erreurs de validation: \n {}".format(messErreurRecu))
            elif response.status_code == 404:
                messErreurRecu = "{0} - {1} : Introuvable, vérifier le ID \n{2}".format(response.reason, response.status_code, messErreurReponse)
                raise ErreurApiDQ("Erreurs de validation: \n {}".format(messErreurRecu))
            
            elif response.status_code == 409:
                messErreurRecu = '{0} - {1} \n {2}'.format(response.reason, response.status_code, messErreurReponse)
                raise ErreurApiDQ("Erreurs de validation: \n {}".format(messErreurRecu))
            
            print('** Probleme avec la requete, cette requete est inexacte:\n {}'.format(url))
            raise ErreurApiDQ('Probleme avec la requete, cette requete est incorrecte:\n {0} \n Erreur : {1} '.format(url, messErreurReponse))
        return response
    
    def getJsonResponseResult(self, url, methode='GET', dictParam=None):
        #{'id': '999xxxxx'}
        if dictParam is None:
            dictParam = {}
        resp = ""
        try:
            if methode == 'GET':
                if self.apiKey:
                    resp = self.http.request('GET', url,  headers={'Authorization': self.apiKey})
                else:
                    resp = self.http.request('GET', url)
            elif methode == 'POST':
                  ## NB utiliser plutot la methode MAJ_postRequest() utilisant request. Meilleur et moins de probleme sur POST
                resp = self.http.request('POST', url, headers={'Authorization': self.apiKey, 'Content-Type':'application/x-www-form-urlencoded'}, fields=dictParam)

        except Exception as e:
            print('oh probleme', e)
            raise

        response = json.loads(resp.data) 
        # todo check encodage... peut etre mettre  response = json.loads(response.decode('utf-8')) 
        succes = response['success'] 
        if not succes:
            typeErreur = response['error']['__type']
            if typeErreur == 'Validation Error':
                messErreurRecu = json.dumps(response['error'], ensure_ascii=False).encode('utf8')
                messErreurRecu = messErreurRecu.decode("utf-8") 
                raise ErreurApiDQ("Erreurs de validation: \n {}".format(messErreurRecu))
            
            elif typeErreur  == 'Authorization Error':
                messErreurRecu = response['error']['message']
                raise ErreurApiDQ("Probleme d'authorisation avec votre jeton acces\n {}".format(messErreurRecu))
            
            print('** Probleme avec la requete, cette requete est inexacte:\n {}'.format(url))
            raise ErreurApiDQ('Probleme avec la requete, cette requete est incorrecte:\n {}'.format(url))
        if response['result'] and 'results' in response['result']:
            return response['result']['results']

        return response['result']

    # def getUserInfo(self):

    def getListOrganizationNameAccesForUser(self, user):
        ### methode qui retourne 2 listes des les organisation ou l'utilisateur a des droits
        ## PARAM user: name or id
        #       return listEdition, listeAdmin

        listAdmin, listEditor = [], []
        urlGet = "{0}organization_list_for_user?id={1}".format(self.urlAction, user)
        response = self.getJsonResponseResult(urlGet)

        for org in response:
            if org['capacity'] == 'admin':
                listAdmin.append(org['name'])
            elif org['capacity'] == 'editor':
                listEditor.append(org['name'])
            elif org['capacity'] == 'member':
                print('acces member')
            else:
                print('capacity user = {}'.format(org['capacity']))

        return listAdmin, listEditor

    def getUser(self, user):
        ### methode retournant un dict avec info user
        ### PARAM user: id ou name user
        urlGet = "{0}user_show?id={1}".format(self.urlAction, user)
        return self.getJsonResponseResult(urlGet)

    def getAllUserListDict(self):
        urlGet = "{0}user_list?".format(self.urlAction)
        return self.getJsonResponseResult(urlGet)
        # print('ici')

    def getUserOrganizationEditor(self):
        ### methode qui renvoie un dict : list dict
        ### return {
        #             'aBoudreau': {'admin': ['mrn', 'orgx'],
        #                        'editor': ['orgx', 'orgy']
        #                        },
                 #   'jmartel' : {'admin': ['mrn', 'orgx'],
        #                        'editor': ['orgx', 'orgy']
        #
        #           }

        listDictUser = self.getAllUserListDict()
        dictUserDroit = {}
        dictSysAdmin = {}
        for userDict in listDictUser:
            if userDict['sysadmin']:
                dictSysAdmin[userDict['name']] = userDict
            else:
                listAdmin, listEditor = self.getListOrganizationNameAccesForUser(userDict['name'])
                # print('icit')
                dictUserDroit[userDict['name']] = {'admin': listAdmin,'editor': listEditor}

        return dictUserDroit

    def getOrganizationUserEditor(self, dictUserOrgEditor=False):
        ### methode qui retourne 2 dict de dict d'organisation : adminUser, editorUser

        dictOrgUserAdmin = {}
        dictOrgUserEditor = {}

        if not dictUserOrgEditor:
            dictUserOrgEditor = self.getUserOrganizationEditor()
        for user, dictEditPermission in dictUserOrgEditor.items():

            for org in dictEditPermission['admin']:
                try:
                    dictOrgUserAdmin[org].append(self.getUser(user))
                except:
                    # print('org pas la')
                    dictOrgUserAdmin[org] = [self.getUser(user)]

            for org in dictEditPermission['editor']:
                try:
                    dictOrgUserEditor[org].append(self.getUser(user))
                except:
                    # print('org pas la')
                    dictOrgUserEditor[org] = [self.getUser(user)]

        # print('ici')
        return dictOrgUserAdmin, dictOrgUserEditor



    def getOrganizationId(self, organizationName):
        """ Methode qui retourne id d'une organisation """
        urlGet = "{0}organization_show?id={1}".format(self.urlAction, organizationName)
        try:
            response = self.getJsonResponseResult(urlGet)
        except Exception as e:
            raise ErreurApiDQ("Le nom de l'organisation {} est invalide.\n"
            "Pour voir les noms d'organisation valide lancer la requete suivante dans votre navigateur internet\n"
            "https://www.donneesquebec.ca/recherche/api/3/action/organization_list?".format(organizationName) )

        id = response['id'] 
        self.idOrganisation = id
        return id


    def getOrganizationsListName(self):
        """Méthode renvoi une list des noms des organisations"""
        urlGet = "{0}organization_list?".format(self.urlAction)

        try:
            response = self.getJsonResponseResult(urlGet)
        except Exception as e:
            raise ErreurApiDQ("problèeme avec requete {}".format(urlGet))

        return  response


    def getOrganizationsDict(self):
        """ Méthode qui renvoie une list de dict de toute les organisations
        """

        orgNameList = self.getOrganizationsListName()

        orgListOfDict = []

        for org in orgNameList:
            urlGet = "{0}organization_show?id={1}".format(self.urlAction, org)
            try:
                response = self.getJsonResponseResult(urlGet)
            except Exception as e:
                raise ErreurApiDQ("problèeme avec requete {}".format(urlGet))

            orgListOfDict.append(response)

        print('ici')
        return orgListOfDict

    def getNewOrgSince(self, listDictOrg, dateSince):
        """ Methode qui a partir d'une listDictOrg retourne celle creer depuis la date en param
        """
        listOrgSince = []
        for org in listDictOrg:
            if org['created'] >  dateSince:
                listOrgSince.append(org)

        return listOrgSince

    def addOrganizationPrincipalFirstLevelInListOfDictOrg(self, listDictOrg):
        #ajouter la donnée du nom de organisation et org principale au premier niv dans la listDictOrg
        # cle orgParent and ajouter au dict au premier niveau
        for aOrg in listDictOrg:
            try:
                orgPrincipale = aOrg['groups'][0]['name']
                aOrg['orgParent'] = orgPrincipale
            except Exception as e:
                # pas d'organizsation principale donc en ai une elle meme
                aOrg['orgParent'] = aOrg['name']

    def addOrganizationNameFirstLevelInListOfDictPack(self, listDictPack):

        """ Méthode qui ajoute au premier niveau du dict des pack, organization name
        nouvelle clé orgName
        """
        for aPack in listDictPack:
            aPack['orgName'] = aPack['organization']['name']

    def addGroupStrFirtLevelInListOfDict(self, listDictPack):
        """ Méthode qui ajoute au premier niveau du dict des pack, une string des groupes
        nouvelle clé groupString
        """

        for aPack in listDictPack:
            listGroups = aPack['groups']
            groupStr = ''
            for aGroup in listGroups:
                groupStr += '{}-'.format(aGroup['title'])
        
            aPack['groupString'] = groupStr[:-1]

    def addNoteWithoutCRLF(self, listDictPack):
        """ Méthode qui retire espace, retour a la ligne dans la description (notes)
        nouvelle clé noteWithoutCRLF
        """

        for aPack in listDictPack:
            note = aPack['notes']
            test = note.replace('\n', '').replace('\r', '').replace('\r\n','')
            # test = note.strip()
            # test = note.split()
            # test2 = ' '.join(test)
            # re.sub(r"[\n\t\s]*",' ',note)
            aPack['noteWithoutCRLF'] = test

    def addUrlPackInListOfDict(self, listDictPack, enProd=True):
        """ Méthode qui ajoute url du jeu au premier niveau dans le dict pack
        nouvelle clé urlPack
        """
        if not enProd:
            raise ErreurApiDQ('le param non en prod n est pas implémenté, modifier url pour bet ou pab, a faire')
        
        for aPack in listDictPack:
            url = 'https://www.donneesquebec.ca/recherche/dataset/'
            name = aPack['name']
            aPack['urlPack'] = url+name

    def addTagString(self, listDictPack):
        """ Méthode qui ajoute une string des tags du jeux
        Nouvelle clé: tagString
        """
        for aPack in listDictPack:
            tagString = ''
            for aTag in aPack['tags']:
                tagString += '{}-'.format(aTag['name'])
            tagString = tagString[-1]
            aPack['tagString'] = tagString


    def getDictResourceTrueCondition(self, resourceAttribut, resourceAttributValue):
    ### methode qui renvoie un dict de resource remplissant les condition
    ## Exemple apel: servApi.getDictResourceTrueCondition(resourceAttribut='format', resourceAttributValue='CSV')
    ## donnera un dict des ressource ou le format = CSV

        dictPackage = self.getAllPackages()
        dictResourcesTrueCondition = {}
        for idPack, pack in dictPackage.items():
            print('ici')
            for resource in pack['resources']:
                if resource[resourceAttribut] == resourceAttributValue:
                    dictResourcesTrueCondition[resource['id']] = resource
        return dictResourcesTrueCondition


    def getPackForIdPack(self, idPackage):
        """Méthode qui retourne le pack de id en parametre
        """
        url = '{}package_show?id={}'.format(self.urlAction, idPackage) #1000 est la limt
        response = self.getJsonResponseResult(url)
        return response

    # rename, old name: getPackageDict
    def getAllPackages(self, returnType='dictOfDict'):
        """Methode qui retourne tout les packages
        returnType:  (dictOfDict, listOfDict)  par default  dictOfDict

        dict = {'id': {} }
        n'inclus pas les pack private

        """

        url = '{}package_search?rows=1000'.format(self.urlAction) #1000 est la limt
        response = self.getJsonResponseResult(url)
        # print('ici')
        leDict = {}
        for unPackage in response:
            leDict[unPackage['id']] = unPackage

        # 2e passe car 1000 package max par requete
        url2 = '{}package_search?rows=1000&start=1000'.format(self.urlAction) #1000 est la limt
        response2 = self.getJsonResponseResult(url2)
        if returnType == 'listOfDict':
            return response+response2

        leDict2 = {}
        for unPackage in response2:
            leDict2[unPackage['id']] = unPackage

        leDict.update(leDict2)

        # if returnType == 'listOfDict':
        #     newListOfDict = self.listOfDictFromDictOfDict(leDict)
        #     return newListOfDict
        
        return leDict


    def getPackageListDictPrivate(self):
        # return: [
        #           {
        #               author:min,
            #           id: xxxx
            #           ...
        #           },
        #           {
            #           author:minChose,
            #           id: yyy
            #           ...
            #        }
        #          ]
        # https: // beta.donneesquebec.ca / recherche / api / 3 / action / package_search?include_private = True & q = private:true

        urlGet = "{0}package_search?rows=1000&include_private=True&q=private:true".format(self.urlAction)

        response = self.getJsonResponseResult(urlGet)
        print('ici')
        return response

    def getPackageListDictForAOrganization(self, organizationName, includePrivate=False):
        """ Methode qui retourne une list de dict de package appartenant a une organization 
            param: organization : peut etre le id ou le nom 
        """

        urlGet = "{0}package_search?q=organization:{1}&rows=1000".format(self.urlAction, organizationName)
        if includePrivate:
            urlGet += "&include_private=True"
        response = self.getJsonResponseResult(urlGet)
        return response

    def getPackagesForStringInTitle(self, stringToFindInTitlePackage, organizationName=False):
        """ Methode qui retourne un dict de package ou STRING se retrouve dedans title package
        """
        dictPack = ''
        if organizationName:
            listDict = self.getPackageListDictForAOrganization(organizationName, includePrivate=True)
            dictPack = self.convertListDict2DictDict(listDict)
        else:
            dictPack = self.getAllPackages() ## return dict de dict

        dictPackTitleFit = {}
        for key, dictVal in dictPack.items():
            title = dictVal['title']
            if stringToFindInTitlePackage in title:
                dictPackTitleFit[key]= dictVal

        return dictPackTitleFit

    def getPackageListDictForDateCreation(self, dateBegin, dateEnd='NOW'):
        ### Méthode qui retourne une list de package creer depuis la date ou entre 2 dates
        # la date en parametre est incluse
        listAllPack = self.getAllPackages(returnType='listOfDict')
        listDictCreateUntilDate = self.filterDictPackCondition(listDictPack=listAllPack, attribut='metadata_created', condition=dateBegin, operateur='>=', attributType='date')

        if dateEnd != 'NOW':
            dictdictPack = self.convertListDict2DictDict(listDictCreateUntilDate)
            listDictCreateUntilDateExclude = self.filterDictPackCondition(listDictPack=listDictCreateUntilDate, attribut='metadata_created', condition=dateEnd, operateur='>=', attributType='date')
            # remove pack from dateEnd
            for packDict in listAllPack:
                for packReject in listDictCreateUntilDateExclude:
                    idPack = packDict['id']
                    idPackReject = packReject['id']
                    if idPack == idPackReject:
                        dictdictPack.pop(idPackReject)


            listDictCreateUntilDate = self.convertDictDict2ListDict(dictdictPack)

        return listDictCreateUntilDate


    def filterDictPackCondition(self, listDictPack, attribut, condition, operateur='==', attributType=''):
        # return listDictPack = [
    #                       {'author':'chose',
    #                       'author_email': 'choseBine',
    #                       ...
    #                       },
    #                       {'author': 'chose2',
        #                       'author_email': 'choseBine2',
        #                       ...
        #                    },
    #                       ]
        # exemple apel:
        # dict de package
        # listDictCreateUntilDate = serv.filterDictPackCondition(listDictPack=listAllPack, attribut='metadata_created', condition=dateBegin, operateur='>=', attributType='date')

        # dictOrganization


        if attributType == 'date':
            condition = self.servUtil.convertStringToDate(condition)

        listPackConditionTrue = []
        if operateur == '==':
            for i in listDictPack:
                attrValue = i[attribut]
                if attributType == 'date':
                    attrValue = self.servUtil.convertStringToDate(i[attribut])
                if attrValue == condition:
                    listPackConditionTrue.append(i)
        elif operateur == '<=':
            for i in listDictPack:
                attrValue = i[attribut]
                if attributType == 'date':
                    attrValue = self.servUtil.convertStringToDate(i[attribut])
                if attrValue <= condition:
                    listPackConditionTrue.append(i)
        elif operateur == '>=':
            for i in listDictPack:
                # if i['author_email'] == 'xxx'
                #     print('ici')
                attrValue = i[attribut]
                if attributType == 'date':
                    attrValue = self.servUtil.convertStringToDate(i[attribut])
                if attrValue >= condition:
                    listPackConditionTrue.append(i)

        return listPackConditionTrue



    def getDatastore_search(self, idResource, limit=100):
        # méthode qui retourne les enregistrement du data store
        # NB fonctionne uniquement sur CSV téléversé au datastore
        # limit est 100 par default et le limit max est 32000

        # https://beta.donneesquebec.ca/api/3/action/datastore_search?resource_id=c5c03a0a-06a4-4afe-87df-5c62bdaa3a60
        if limit == 'max':
            limit = 32000

        url =  self.urlAction+"datastore_search?resource_id={0}&limit={1}".format(idResource, limit)
        response = self.getJsonResponseResult(url, methode='GET', dictParam={'id': idResource} )
      
        # entité dans   response['records']

        return response

    def getDatastore_dict_meta(self, idResource):
        # return (dict) {'NomChamp': 'Type'} , (int)nbLigne in datastore
        # Exemple:  {'NomLegal': 'text', 'NoDossier': 'number', 'PUBLIE': 'text', 'NEQ': 'text', 'DateCons': 'text'}, 200
        url =  self.urlAction+"datastore_info".format()
        response = self.getJsonResponseResult(url, methode='POST', dictParam={'id': idResource} )

        dictionnaire = response['schema']
        metaNbLigne = response['meta']
        return dictionnaire, metaNbLigne

    def getDatastore_nb_line(self, idResource):
        dictMeta, nbLine = self.getDatastore_dict_meta(idResource)
        return nbLine

    def listOfListFromListOfDict(self, listDict, listAttribut):

        listOfList = []
        for dictionary in listDict:
            myList = []
            for att in listAttribut:
                attToAppend = dictionary['{}'.format(att)]
                myList.append(attToAppend)
            listOfList.append(myList)
        return listOfList

    def listOfDictFromDictOfDict(self, dictOfDict):

        print('ici')
        newListOfDict = []
        for key, elem in dictOfDict.items():
            newListOfDict.append(elem)

        return newListOfDict


    def writeJsonDictOfDictPackage(self, path, dictOfDictPackage):
        ### methode ecrivant un JSON par dict a partir d'un dictionnaire de dictionnaire
        for key, dictVal in dictOfDictPackage.items():
            self.writeJsonDict(dictVal, path, dictVal['name'])


    def writeJsonDict(self, dictPackage, path, jsonFileName, overwriteFile=True):
        ## path : path/to/file/no/jsonFileName.json
        ## jsonFileName: test


        # jsonFileToSaveName = "{0}/pack_{1}.json".format(path, dictPackage['name']) ## name= title avec tiret sans espace et carac accentué
        jsonFileToSave = "{0}/{1}_{2}.json".format(path, jsonFileName, self.getDateHeure_actuelle(heure=False)) ## name= title avec tiret sans espace et carac accentué

        with open(jsonFileToSave, 'w',  encoding="utf-8" ) as jsonFile:
            json.dump(dictPackage, jsonFile, ensure_ascii=False)

    def getDictFromJsonRead(self, pathJsonFile):

        with open(pathJsonFile, "r", encoding='utf-8') as read_file:
            print("Reading JSON serialized Unicode data from file")
            sampleData = json.load(read_file)
            # print('check ok')
            return sampleData

    def MAJ_deletePackage(self, idPackToDelete, purge=False):
        """ Méthode qui supprime un package
        *  purge = remove completement ** attention cannot by undone **

        """
        dictId = {'id': idPackToDelete} # pour delete un pack on fait un dict avec le id
        urlPost = "{0}package_delete".format(self.urlAction)
 
        self.MAJ_postRequest(url=urlPost, dictMaj=dictId)
        
        if purge:
            urlPurge = "{0}dataset_purge".format(self.urlAction)
            self.MAJ_postRequest(url=urlPurge, dictMaj=dictId)

        print('package {} a été supprimé'.format(idPackToDelete))


    def MAJ_createPackage(self, dictPackToCreate):
        """ Méthode qui creer un package a partir d'un dict
        Exemple dict pack DQ avec attributs obligatoires 
        Exemple dictMaj creation Pack = 
        {
            'title': 'test creation',
            'notes': 'A long description of my dataset',
            'update_frequency': 'hourly',
            'ext_spatial': 'cm-montreal',
            'owner_org': '3ddeaf63-a0e9-4f46-8bfc-e789e14042e1', #mcn
            'extras_organisation_principale': "gouvernement-du-quebec",
            'language': 'FR',
            'license_id': 'cc-by',
            'name': 'test-creation', # attention avec ceci, ce sera id-url pour acces API donc respecter les obligations acces url 
                                        ## -> pas accent, carac spéciaux, pas espaces, on separe avec des tierets, doit réfléter le titre
            
            ### att optionels
            'inv_key_data': 'key_data', # optionel
            'inv_related_laws': [{'inv_related_law': 'G-1.03 Loi sur la gouvernance et la gestion des ressources informationnelles des organismes publics et des entreprises du gouvernement'}]
        }
    """
        urlCreationPack = "{0}package_create".format(self.urlAction)

        packCreate = self.MAJ_postRequest(url=urlCreationPack, dictMaj=dictPackToCreate)
        return packCreate
    
    def MAJ_resourceAttribut(self, dictResourceChangeIdNewVal, attributName, attributToChangeIsADate=False):
        """
        param dictResourceChangeIdNewVal: dict = {'id' : dictRessource}
        
        """

        for resourceId, newValue in dictResourceChangeIdNewVal.items():
            if attributToChangeIsADate:
                newValue = self.convertDateEDTToUTC(newValue)
                #todo marche pas indentation...
            try:
                postResponse = self.http.request('POST', self.urlPatch, headers = self.postHeader, fields={'id': resourceId, attributName:newValue})
                post_resultData = postResponse.data
                
            except IOError as e: 
                print (" le message d'erreur: ", str(e))
                raise ErreurApiDQ('problème dans le lancement de la requete POST: {}'.format(e))
            
            else:
                post_dict = json.loads(post_resultData.decode('utf-8')) # résultat de l'execution de request
                success = post_dict['success']

                if not success:
                    messErreur = post_dict['error']['message']
                    if 'Action resource_patch requires an authenticated user' in messErreur:
                        raise ErreurApiDQ("Traitement interrompu. La clé API saisie est incorect, la clé ne correspond pas a un utilisateur valide")
                    elif 'Accès non autorisé' in messErreur:
                        raise ErreurApiDQ("Traitement interrompu. {}. ".format(messErreur)) # pas les droits de modif...
                    elif 'Introuvable' in messErreur:
                        raise ErreurApiDQ('Traitement interrompu. ce id de ressource est introuvable: {}'.format(resourceId))
                    else: 
                        raise ErreurApiDQ('Traitement interrompu. Problème dans le lancement de la requete POST: {}'.format(messErreur))
                else:
                    print ('Succès', resourceId)

    def MAJ_packDate_created(self, idPack, date):
        """ methode modifiant la date de creation d'un package
            attention à la date passée en parametre, la date sera convertie en UTC, si pas d'heure, 
            l'heure actuelle sera inscrite
        """
        print ("les modifications des dates ne fonctionne pas 🙁")
        print ("1: la date de creation ne peut être modifié meme avec des droit sysadmin")
        print ("2: la date de modif du pack se met automatiquement a jour avec la date de l'intervention de api donc impossible mettre autre chose.")
        # dateUTC = self.convertDateEDTToUTC(date)
        # dictPack = {
        #     'id' : idPack,
        #     'date_created': dateUTC,
        #     'date_modified': dateUTC
        #     'methodologie': 'test'
        # }
        
        # urlPackPatch = "https://www.donneesquebec.ca/recherche/api/3/action/package_patch" 

        # response = self.MAJ_postRequest(self.urlPackPatch, dictPack)
        # print('ici')


    def csvToDictResourceChangeIdNewVal(self, pathFileCSV, attributToChange):
        """ Méthode qui prend les colonnes id et nomAttributToChange d'un CSV et renvoie un dictionnaire 
            key: id, value: valeur attribut a modifie
        Exemple pour attribut last_modified: 
        {'82843390-821d-4f9f-af37-e3b876de00e8': '2021-08-20T12:00:00'}
        """
        import csv
        dictResourceIdNewVal = {}
        newValColName = 'new_{}'.format(attributToChange)
        with open(pathFileCSV, newline='') as csvfile:
            csvReader = csv.DictReader(csvfile, delimiter=';')
            for row in csvReader:
                try:
                    if row[newValColName] and not row[newValColName].isspace() :
                        dictResourceIdNewVal[row['id']] = row[newValColName]
                except Exception as e:
                     raise ErreurApiDQ("Traitement interrompu: La colonne '{}' n'existe pas dans le fichier CSV".format(e))
        return dictResourceIdNewVal


    def writeCSVResourceFromDict(self, listDictPackage, pathFileSortieCSV, encoding="utf-8", AttributNameFuturChange=''):
        """ Methode qui écrit un csv de chaque ressource presente dans le dict de package 
            param: listDictPackage : une liste de dictionnaires provenant de methode  getPackageListDictForAOrganization('mffp')
        """

        import csv
        try:
            with open(pathFileSortieCSV, mode='w', encoding=encoding) as csv_file:
                fieldNames = ['package_name','new_{}'.format(AttributNameFuturChange), 'last_modified', 'name', 'format', 'description', 'id', 'url']
                writer = csv.DictWriter(csv_file, fieldnames=fieldNames, delimiter = ";")
                writer.writeheader()
                for currentPackage in listDictPackage:
                    try: 
                        currentOwner_org = currentPackage['owner_org']
                    except: 
                        continue
                    if currentOwner_org != self.idOrganisation:
                        print('biz en fou')
                    
                    packName = currentPackage['name']
                    resources = currentPackage['resources']

                    for resource in resources:
                        if resource['id'] == '824536c1-739b-4581-b09d-d7498f874423':
                            print('check probleme')

                        resource['package_name'] = packName
                        # Retrait des , pour que le parser de CSV au cas ou quelqu'un choisirait , comme separateur...
                        resource['name'] = resource['name'] .replace(';', '')
                        resource['description'] = resource['description'] .replace(',', '')
                        resource['description'] = resource['description'] .replace(';', '')

                        dateUTC = resource['last_modified']
                        if not dateUTC:
                            # s'il n'y a pas de date de modification, on met la date de creation, comme dans l'interface web DQ
                            dateUTC = resource['created']

                        dateEDT = self.convertDateUTCToEDT(dateUTC)
                        resource['last_modified'] = dateEDT

                        data = {key: value for key, value in resource.items() if key in fieldNames}
                        writer.writerow(data)

        except Exception as e:
            if type(e) == PermissionError:
                raise ErreurApiDQ("Probleme a l'ouverture du fichier csv. le fichier est ouvert dans une autre application: {}".format(pathFileSortieCSV))
            elif type(e) == ErreurApiDQ:
                raise e
            else:
                ErreurApiDQ("Probleme a l'ouverture du fichier csv: {}.".format(pathFileSortieCSV))


    def writeCSVInfoResourceForOrganisation(self, organisationName, pathFileSortieCSV, encodingCSV, attributForFuturChange):
        orgId = self.getOrganizationId(organisationName) # a faire leve une erreur si le name est pas valide
        listDictPackageForOrganization = self.getPackageListDictForAOrganization(organisationName)
        self.writeCSVResourceFromDict(listDictPackageForOrganization, pathFileSortieCSV, encodingCSV, attributForFuturChange)


if __name__ == '__main__':
    "to test class Service API"

    from JETONSJOSEE import JETONSJOSEE
    # jeton = JETONSJOSEE['BETA']
    jeton = JETONSJOSEE['BETA']
    objService = ServiceAPI_DQ(jeton, enTest=True)
    # objService = ServiceAPI_DQ(jeton, enTest=False)
    from serviceCsv import *
    servCsv = ServiceCSV('C:/temp/outputJeuDonne.csv')


    objService.MAJ_packDate_created(idPack=	'2a928805-5dd5-4e38-bb52-997a86a4fa22', date='2024-10-10')
    # objService.MAJ_packDate_created(idPack=	'd7088cdb-ab14-4ca7-9127-eeefdbd49352', date='2024-10-10')

    ###########
    path = "C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/output/"
    # DictResult = objService.getPackagesForStringInTitle(stringToFindInTitlePackage='pilote', organizationName='mcn')
    # objService.writeJsonDictOfDictPackage(path, DictResult)
    #
    # pathJson = "C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/output/pack_dossier-d-assiduite-conforme.json"
    # aDict = objService.getDictFromJsonRead(pathJson)
    ##############

    ##################
    # test user droit acces admin-edition
    ##############
    # userList = objService.getUserOrganizationEditor()
    # dictOrgUserAdmin, dictOrgUserEditor = objService.getOrganizationUserEditor(userList)
    # objService.writeJsonDictPackage(dictOrgUserAdmin, path, 'orgAdmin')
    # objService.writeJsonDictPackage(dictOrgUserEditor, path, 'orgEditor')


    ##############
    #  get pack private
    ##############

    # result = objService.getPackageListDictPrivate()


    ###########
    # get pack of a org
    #############

    # packMrn = objService.getPackageListDictForAOrganization(organizationName='mrn')
    # packTrueCondition = objService.filterDictPackCondition(listDictPack=packMrn, attribut='metadata_created', condition='2023-03-31', operateur='<=')
    #
    #
    # listOfListAttribut = objService.listOfListFromListOfDict(packTrueCondition, ['author', 'metadata_created', 'name', 'id'])
    # listOfListAttribut.insert(0, ['Responsable', 'date_cree', 'nom_jeu', 'id_jeu'])
    #
    # servCsv.ecrireCSVFromListOfList(listOfListAttribut)
    #
    # print('ici')
    ###################
    #get pack for a id ressource
    ##########

    # leDict = objService.getPackForIdPack('c937deb0-a089-4035-88f1-06b36bfd42c5')
    # org = leDict['organization']['name']
    # print('ici')


    # dictMeta, nbLine = objService.getDatastore_dict_meta('988e0b0a-2803-4ed7-b00f-c399d18dc589')
    # test = objService.getDatastore_nb_line('988e0b0a-2803-4ed7-b00f-c399d18dc589')


    # dateEDT = objService.convertDateUTCToEDT('2021-06-21T16:22:22.222222')
    # dateEDT = objService.convertDateUTCToEDT('2021-06-28T13:39:50')
    #
    # dateUTC = objService.convertDateEDTToUTC('2021-06-28')
    # dateUTC = objService.convertDateEDTToUTC('2021-06-28T09:39:50')
    #
    # orgId = objService.getOrganizationId('mrn')
    # listDictPackageForOrganization = objService.getPackageListDictForAOrganization('developpement-durable-environnement-et-lutte-contre-les-changements-climatiques')
    # objService.writeCSVResourceFromDict(listDictPackageForOrganization, 'C:/job/a_temp/dqTest', 'utf-8')



    print('icit')