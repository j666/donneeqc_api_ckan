

import os, io
from datetime import datetime


class ServiceTxtNew:

    def __init__(self, pathNomFichier, ecraserFile=True):


        dirPath = os.path.dirname(pathNomFichier)
        if not os.path.exists(dirPath):
            raise Exception("Le chemin acces au fichier txt n'existe pas: {}".format(pathNomFichier))

        if ecraserFile:
            if os.path.exists(pathNomFichier):
                try:
                    os.remove(pathNomFichier)
                except Exception as e:
                    # print('ok existe pas')
                    pass


        self.pathFil = pathNomFichier
        self.leFil = io.open(pathNomFichier, "a", encoding="utf-8")

        # self.typeVerif = typeVerif



    def __str__(self):
        return repr(self.pathFil)

    def __repr__(self):
        """Redefinit comment on affiche a l'ecran.
        """
        return "Objet qui fait des actions sur des fichiers textes "

    def __del__(self):
        try:
            self.leFil.close()
        except Exception as e:
            pass
        # print("close le fil")
        # print("\033[35m{}".format("close le fil"))
        # print(self.leFil.closed)

        # changeCouleurPrint('', 'default')

    def date_formate(self):
        """ Fonction qui retourne date et heure:
         Exemple: 20161028 11h50min22sec
        """

        date_time = datetime.now()
        date_format = date_time.strftime("%Y%m%d_%Hh%Mmin%Ssec")
        return date_format

    def ecrire(self, message):

        # changeCouleurPrint(message, 'rouge')
        # changeCouleurPrint('', 'default')
        try:
            message += "\n"
            self.leFil.write(message)
        except Exception as e:

            byteEncode = message.encode()
            strDecodeUTF = byteEncode.decode('utf-8-sig')
            self.leFil.write(strDecodeUTF)



    def creer_fichier_ecrire_entete(self, textEntete, date='now'):
        """
            Fonction qui écrit l'entete dans le fichier
            """
        if date == 'now':
            date = self.date_formate()

        self.leFil = io.open(self.pathFil, "a")

        self.leFil.write("\n{0}  - Date: {1} \n".format(textEntete.upper(), date))
        self.leFil.write("\n_____________________________________________________________\n\n")

        # self.leFil.close()


    #
    #
    def ecrire_ligne_txt(self, message):

        self.leFil = io.open(self.pathFil, "a")

        self.leFil.write(message)
        self.leFil.write("\n")
        # self.leFil.close()

