

# Type dictionaire ->  Clé : valeur

monDictionnaire = { "chien": "un mammifaire qui aboit beaucoup",
                   "chat": "mammifere tres indépendant",
                   "pomme": "fruits parce qu'il a des pepins",
                   }



print(monDictionnaire['chat'])

x = monDictionnaire.keys()
y = monDictionnaire.values()

z = monDictionnaire.items()


# parcourir dictionaire 
for unElement in monDictionnaire:
    print(unElement)

print('ok?')

# boucle sur les cle facon 2
for cle in monDictionnaire.keys():
    print(cle)

# for valeur:
for val in monDictionnaire.values():
    print(cle)
print('ok?')


for cle, valeur in monDictionnaire.items():
    print(cle, valeur)

print('ok?')


# change valeur de pomme
monDictionnaire['pomme'] = 'un fruit!!'


# change une cle = non pas possible

# ajouter un item au dict
monDictionnaire['tomate'] = 'un legFru'


# supprimer un item au dict
del monDictionnaire['pomme'] 





