
uneListe = ['pomme', 'poire', 'ananas']
             
### liste index

le3eFruit = uneListe[2]
print(le3eFruit)

print(uneListe[1]) # index


### methode des objects list


uneListe.append('melon')
print(uneListe)

uneListe.pop(1)
print(uneListe)

uneListe.sort()
print(uneListe)

uneListe.sort(reverse=True)
print(uneListe)


print(uneListe.count('pomme')) # compter les element = pomme


# list of list
pays_capitales = [ [ "France", "Uruguay", "Allemagne", "Ghana" ], [ "Paris", "Montevideo", "Berlin", "Accra" ]]
la3eCapitaleDe2eListePays = pays_capitales[1][3]

print(la3eCapitaleDe2eListePays)

pays_capitales[0].append('Canada') # ajouter l'element Canada a la premiere liste
print(pays_capitales[0])

pays_capitales[0].remove('Canada') # retirer un element de la premiere liste
print(pays_capitales[0])







