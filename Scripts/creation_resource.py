
## Josée Martel 202502


import json, urllib, requests
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
# sys.path.append("../donneeqc_api_ckan")
####
# Retrouver les informations de votre jeton
# ** Ne jamais mettre votre jeton sur un espace partagé ou nuage comme un dépot GIT


from JETONSJOSEE import JETONSJOSEE
### JETONSJOSEEEDITEUR = {
###    "PROD":"xxxxxxx",
###    "BETA" : "xxxxyyyy",
###    "PAB" : "xxxxyyzzzz"}

jeton = JETONSJOSEE['PROD'] # environnement Prod
# jetonPAB = JETONSJOSEE['PAB'] # environnement PAB
postHeader = {
            "Authorization": jeton,
            'Content-Type':'application/x-www-form-urlencoded',
            "Connection": "keep-alive" }


if __name__ == '__main__':


    #################
    # Creer une ressource à l'intérieur du package josee-test-creation
    #################

    urlActionPAB = "https://www.donneesquebec.ca/recherche/api/3/action/" 
    dictResourceACreer = {

        'package_id': '5947d561-74c8-4d44-bb7c-a51e91b3d53b', # MAMH test PROD
        # 'package_id': 'josee-test-creation',
        'name' : '3resssource_cree_api', 
        'description' : 'lien vers CSV contenant xyz',
        'taille_entier': 1, 
        'format':'CSV',
        'resource_type': 'donnees',  
        'relidi_condon_valinc':'oui', 
        'relidi_condon_nombre':'n/a',
        'relidi_condon_boolee':'oui',
        'relidi_condon_datheu':'oui',
        'relidi_confic_utf8':'oui', 
        'relidi_confic_separateur_virgule':'oui',
        'relidi_confic_pascom':'n/a',
        'relidi_confic_epsg':'n/a', 
        'url_type': None,
        'mimetype': 'text/csv',
        'mimetype_inner': None,
        'url': 'https://donneesouvertes.affmunqc.net/profilfinancier/pf-mun-2023-2023.csv'

        }


    urlCreationResource = urlActionPAB+"resource_create"
    
    response = requests.post(urlCreationResource, 
                            data=dictResourceACreer, 
                            headers=postHeader)
                            # files= fichier_a_envoyer)

    if response.status_code == 200:
        print('Ressource créée avec succès.')
    else:
        print('Erreur lors de la création de la ressource :', response.text)



    print('Terminé 🍾🍾🍾🍾🍾🍾')











