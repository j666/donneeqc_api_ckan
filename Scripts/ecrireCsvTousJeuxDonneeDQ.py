
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")


from services.serviceAPI_DQ import *
from services.serviceCsv import *
import re


if __name__ == '__main__':
    # JM 2023-11
    ### Script qui écrit 1 CSV de tous les jeux DQ

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']
    servDQ = ServiceAPI_DQ(jeton, enTest=False)
    # servDQ = ServiceAPI_DQ(jeton, enTest=False, environnement="MTL")
    # servDQ = ServiceAPI_DQ(jeton, enTest=False)

    allPackListOfDict = servDQ.getAllPackages(returnType="listOfDict")

    fileCsvError = "C:/Users/marjo31/local_job/temp/{}_allDQPack.csv".format(servDQ.getDateHeure_actuelle(heure=False))
    servCSV = ServiceCSV(fileCsvError)

    servDQ.addOrganizationNameFirstLevelInListOfDictPack(allPackListOfDict)
    servDQ.addGroupStrFirtLevelInListOfDict(allPackListOfDict)
    servDQ.addNoteWithoutCRLF(allPackListOfDict)

    # colToWrite = [ 'name', 'title','metadata_created', 'groupString', 'orgName', 'noteWithoutCRLF']
    # colToWrite = [ 'title', 'name', 'metadata_created', 'orgName']
    colToWrite = [ 'title', 'name', 'metadata_created', 'orgName', 'num_resources','private']
    # colToWrite = [ 'name', 'metadata_created', 'orgName', 'notes' ] todo: les note on des retour a la ligne... a enlever
    listOfListToWrite = servCSV.getListOfListToWrite(inputToWrite=allPackListOfDict, listColumn=colToWrite, inputType='listOfDict')
    # add organisation...

    servCSV.ecrireCSVFromListOfList(listOfListToWrite)

    print('terminé, csv des pack sauvegardé')