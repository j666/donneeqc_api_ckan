
# coding: utf-8

import urllib3
# import requests
import json
import pprint
import datetime
import sys

def getUrlAsJson(url):
    resp = http.request('GET', url)
    return json.loads(resp.data)

# Disable warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

http = urllib3.PoolManager()


mffp_owner_org_id = '46e45b23-9038-4686-a68d-f57caeac880f'
# mtq_owner_org_id = 'fb1886c9-2993-46e0-9a10-f9b938a7826f'

urlGetPackageMTQList='https://www.donneesquebec.ca/recherche/api/3/action/package_search?q=organization:mffp&rows=1000'
# urlGetPackageMTQList='https://www.donneesquebec.ca/recherche/api/3/action/package_search?q=organization:mtq&rows=1000'
urlGetPackage_list = 'https://www.donneesquebec.ca/recherche/api/3/action/package_list'
urlGetPackage_show = 'https://www.donneesquebec.ca/recherche/api/3/action/package_show'

urlOrganizationList = 'https://www.donneesquebec.ca/recherche/api/3/action/organization_list'

package_list_response = getUrlAsJson(urlGetPackageMTQList)
package_list = package_list_response['result']['results'] 
successPackageList = package_list_response['success'] 

organizationInfo = getUrlAsJson(urlOrganizationList)
orgResponse = organizationInfo['result']
print('ici')

if not successPackageList:
    sys.exit()

li_ressource_id_mtq = []
dict_id = {}
dict_nom_format = {}
print(len(package_list))
for currentPackage in package_list:

    try: 
        currentOwner_org = currentPackage['owner_org']
    except: 
        continue
    if currentOwner_org == mffp_owner_org_id:
        ##print(package, packageUrl)
        packName = currentPackage['name']
        resources = currentPackage['resources']

        for resource in resources:
            # if resource['format'] in ('CSV','GeoJSON','SHP','GPKG','WMS','WFS'):
                #print(resource) 
            # li_ressource_id_mtq.append(resource['id'])
            name = resource['name']
            format = resource['format']
            url = resource['url']
            list = [name, format, url]
            id = resource['id']

            dict_id[resource['id']] = list
            dict_nom_format["{0}_{1}".format(resource['name'],resource['format'])] = [id, url, packName] ## verif le nb est pas bon!!!!

print(li_ressource_id_mtq)









    



































sys.exit()

# initialiser la clé API
apikey = '79225f04-889e-4c40-8f3f-cf519d35401b' # Remplacer par votre clé API que vous trouverez sur page de votre compte de Données Québec

# user-agent to by-pass the anti-scraping
headers={
        "Authorization":apikey,
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "Sec-Fetch-Dest": "document",
        "Referer": "https://www.google.com/",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9"
        }

lien ='https://www.donneesquebec.ca/recherche/api/3/action/resource_patch' #API CKAN pour modifier une ressource resource_patch

# idendifiant des ressoures: 
# ICI CRH EN CONTINUE
liste_id_ressources= ['878ec199-1698-4059-b14f-1697fe441070','3e2253e0-56a9-496d-be75-429c5f923bf3','7066def3-c553-4a3e-ae4c-33da6690f48b','93f59ffc-55bc-40b7-bb70-3df7af51aafd'] # liste des ressources dont la date doit être à jour 

last_modified_date = datetime.datetime.utcnow()
date_derniere_maj =last_modified_date.strftime("%Y-%m-%dT%H:%M:%S.%f") # date actuel dans le format de Données Québec


print('******************START RESOURCE PATCH**********************')

print('\n')
print("*****************Modififer la date de dernière modification de la ressource*****************")

for resource_id in liste_id_ressources:
       
    try:
        post3 = http.request('POST',lien, headers = headers, fields={'id': resource_id,'last_modified':date_derniere_maj})
        post_result3 = post3.data
        
    except IOError as e: 
        
        print (" le message d'erreur: ", str(e))
    
    else:
        post_dict = json.loads(post_result3.decode('utf-8')) # résultat de l'execution de request
        print(post_dict['success'])
        print ('\n')
        pprint.pprint(post_dict)


print('\n')
print('*********END RESOURCE PATCH ************')
print ("*******Mise à jour de date terminé********")

