
import urllib3
import json
import csv

# Disable security warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Get the list of all dataset of the portail.
http = urllib3.PoolManager()
req = http.request('GET', 'https://www.donneesquebec.ca/recherche/api/3/action/package_list')

# Make the HTTP request.
response = req.data
# assert response.code == 200

# Use the json module to load CKAN's response into a dictionary.
response_dict = json.loads(response.decode('utf-8'))
assert response_dict['success'] is True
datasets = response_dict['result']
print(datasets)

