
# coding: utf-8

# In[1]:


import urllib3
import requests
import json
import pprint
import datetime


# Disable warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# 
http = urllib3.PoolManager()

##Action## Indiquer votre clé API
apikey = 'xxxxxxxxxxxxxxxxxxxxx'#Remplacer par votre clé API 

# Entête de l'appel http
headers={
        "Authorization":apikey,
        }
# URL incluant l'API
lien ='https://www.donneesquebec.ca/recherche/api/3/action/resource_patch' #API CKAN pour modifier une ressource resource_patch

##Action## Indiquer l'idendifiant de la ressoures à mettre à jour  
resource_id = '59c6ca63-0c94-4261-be77-51aa360d0f2c'#remplacer par l'identifiant de la ressource dont le fichier a été MAJ

# obtention de la date du jour 
now = datetime.datetime.now()
last_modified_date = now + datetime.timedelta(hours = 5)  # Ajout de 5 hrs pour avoir la heure EST, par defaut l'heure fournie est en UTC
date_derniere_maj =last_modified_date.strftime("%Y-%m-%dT%H:%M:%S") # date actuel dans le format de Données Québec

##Action## Date manuelle
date_string = '2020-12-07T13:39:50' # Saisir la bonne date et heure en suivant le format et donner la valeur «True» à date_manuelle
date_derniere_modif = datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S")
date_derniere_modif = date_derniere_modif.strftime("%Y-%m-%dT%H:%M:%S")

##Action## Indiquer quelle date chosir
date_manuelle = True  # «True» pour la choix de la date manuelle ou «False» pour la date du jour obtenue mecaniquement
    
if date_manuelle == False :
    date_maj = date_derniere_modif
else:
    date_maj = date_derniere_maj
    

print('******************START RESOURCE PATCH**********************')

print('\n')
print("*****************Modififer la date de dernière modification de la ressource*****************")
print('\n')

try:
    post3 = http.request('POST',lien, headers = headers, fields={'id': resource_id,'last_modified':date_maj})
    post_result3 = post3.data
        
except IOError as e: 
    
    print (" le message d'erreur: ", str(e))  # affichage de l'erreur si échec
    
else:
    post_dict = json.loads(post_result3.decode('utf-8')) # résultat de l'execution de request
    print(post_dict['success'])
    print ('\n')
    pprint.pprint(post_dict)  #Affichage du résultat si succès

    


print('\n')
print('*********END RESOURCE PATCH ************')
print ("*******Mise à jour de date terminé********")



