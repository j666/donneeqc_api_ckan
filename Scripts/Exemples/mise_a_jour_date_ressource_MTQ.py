
# coding: utf-8

from getopt import getopt, GetoptError
import urllib3
##import requests
import json
##import pprint
from sys import argv, exit
from datetime import datetime
from pprint import pprint



def main(argv):
    print("=======================================================================================")
    print("=======================================================================================")
    print("")
    print("Mise à jour de la date de modification de la ressource")
    print("Version 0.1")
    print("Contact: geomatique@transports.gouv.qc.ca")
    print("En date du 4 février 2021")
    print("")
    print("=======================================================================================")
    print("=======================================================================================")
    apikey = None
    liste_id_ressources= None
    dateUpdate = None
    sep = ','

    # try:
    #     optsEtArg = getopt(argv, "h:a:r:d", ["help", "apikey=","resources-ids=","date="])
    #     opts = optsEtArg[0]
    # except GetoptError as err:
    #     print(err)  # will print something like "option -a not recognized"
    #     print('majDateCkanParRessource.exe --apikey <clé de votre organisation> --resources-ids <id de ressources séparés par ,> --date <date locale au format ISO  >')
    #     exit(2)
    # for opt, arg in opts:
    #     if opt in ("-h", "--help"):
    #         print('majDateCkanParRessource.exe --apikey <clé de votre organisation> --resources-ids <id de ressources séparés par ,> --date <date locale au format ISO  ex: 2021-02-04T13:29:42 >')
    #         exit()
    #     elif opt in ("-a", "--apikey"):
    #         apikey = arg
    #     elif opt in ("-r", "--resources-ids"):
    #         liste_id_ressources = arg.split(sep)
    #     elif opt in ("--date"):
    #         dateUpdate = arg

    # inputManquant = False

    # if not apikey:
    #     print("Veuillez la clé d'api de votre organisation avec la balise --a  ou -apikey")
    #     inputManquant = True
    # if not liste_id_ressources or len(liste_id_ressources) == 0:
    #     print("Veuillez définir une liste d'identifiant de ressources, séparée par des '{0}'  avec la balise -r ou --resources-ids".format(sep))
    #     inputManquant = True
    # if not dateUpdate:
    #     print("La date courante/heure sera utilisée. Vous pouvez définir une date avec la balise --date . Ex: --date 2021-02-04T18:17:42")
    #     dateUpdate = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")

    # if inputManquant:
    #     exit()
    # a enlever



    apikey = '' # Remplacer par votre clé API que vous trouverez sur page de votre compte de Données Québec
    # apikey = '1e0eaaff-4eba-4cca-9291-cc071aeb973e' # Remplacer par votre clé API que vous trouverez sur page de votre compte de Données Québec
    liste_id_ressources =  ['82843390-821d-4f9f-af37-e3b876de00e8']
    dateUpdate = '2018-02-04T09:00:00'


 # JM pertinent a conserve! dans donnée on met time UTC, dans la fiche l'interface sera présenté EDT
    UTC_OFFSET_TIMEDELTA = datetime.utcnow() - datetime.now()
    a = datetime.fromisoformat(dateUpdate) + UTC_OFFSET_TIMEDELTA
    dateUpdateUTC = a.strftime("%Y-%m-%dT%H:%M:%S.%f")


    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    # 
    http = urllib3.PoolManager()

    
    # user-agent to by-pass the anti-scraping
    headers={
            "Authorization":apikey,
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Sec-Fetch-Site": "same-origin",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-User": "?1",
            "Sec-Fetch-Dest": "document",
            "Referer": "https://www.google.com/",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9"
            }

    lien ='https://www.donneesquebec.ca/recherche/api/3/action/resource_patch' #API CKAN pour modifier une ressource resource_patch

    for resource_id in liste_id_ressources:
        
        try:
            post3 = http.request('POST',lien, headers = headers, fields={'id': resource_id,'last_modified':dateUpdateUTC})
            post_result3 = post3.data
            
        except IOError as e: 
            print (" le message d'erreur: ", str(e))
        
        else:
            post_dict = json.loads(post_result3.decode('utf-8')) # résultat de l'execution de request
            success = post_dict['success']

            if not success:
                print ('Échec', resource_id)
                print ('\n')
                pprint(post_dict)
            else:
                print ('Succès', resource_id)

    print('\n')
    print('*********END RESOURCE PATCH ************')
    print ("*******Mise à jour de date terminé********")




if __name__ == '__main__':
    main(argv[1:])