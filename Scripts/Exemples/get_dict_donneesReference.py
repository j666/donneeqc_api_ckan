
#!/usr/bin/env python
import urllib3
import json
import csv
from Scripts.DICT_DATA_ID import *

# Disable security warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Get the list of all dataset of the portail.#
http = urllib3.PoolManager()
CleAPI= "xxxx"
# identifiant_ressource="19385b4e-5503-4330-9e59-f998f5918363"
# identifiant_ressource="0fab8d3e-7b40-4eb9-a212-286e5c404a24" #doc ISBN

for id_ressource, value in DICT_REFERENCE.items():
    url = 'https://www.donneesquebec.ca/recherche/api/3/action/datastore_info'
    req = http.request( 'POST', url,headers ={'Authorization':CleAPI}, fields={'id':id_ressource} )

    # Make the HTTP request.
    response = req.data

    # Use the json module to load CKAN's response into a dictionary.
    response_dict = json.loads(response.decode('utf-8'))
    assert response_dict['success'] is True

    dictionnaire = response_dict['result']['schema']
    metaNbLigne = response_dict['result']['meta']
    print(dictionnaire)

