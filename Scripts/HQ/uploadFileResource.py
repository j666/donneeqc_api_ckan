import urllib3, json, sys
http = urllib3.PoolManager()

## Josée Martel 2024-02
## formation  HackQC24

import json, urllib, requests

####
# Retrouver les informations de votre jeton
# ** Ne jamais mettre votre jeton sur un espace partagé ou nuage comme un dépot GIT
sys.path.append("../donneeqc_api_ckan")
from JETONSJOSEE import JETONSJOSEE
###    "PROD":"xxxxxxx",
###    "BETA" : "xxxxyyyy",
###    "PAB" : "xxxxyyzzzz"}

jetonPAB = JETONSJOSEE['PROD'] # environnement PAB
# jetonPAB = JETONSJOSEE['PAB'] # environnement PAB
postHeader = {
            "Authorization": jetonPAB,
            'Content-Type':'application/x-www-form-urlencoded',
            "Connection": "keep-alive" }

def getJsonResponseResult(url, methode='GET'):
    # fonction générique pour des get à l'API HQ
    response = ""
    try:
        methode == 'GET'
        response = http.request('GET', url)
        reponseLoad = json.loads(response.data) # transforme la reponse en dictionnaire Python (json) 
        if response.status != 200:
            print('** Probleme avec la requete, cette requete est inexacte:\n {}'.format(url))
            raise Exception('Probleme avec la requete, cette requete est incorrecte:\n {}'.format(url))
        
        return reponseLoad

    except Exception as e:
        print('oh probleme', e)
        raise



if __name__ == '__main__':

    urlActionPAB = "https://pab.donneesquebec.ca/recherche/api/3/action/" 
    urlresourcePatch = urlActionPAB+'resource_patch'

    # charger le CSV a ajouter et/ou modifier 
    # fileRessource = "C:/Users/marjo31/local_job/test.csv"
    urlDownCSVHQ = "https://donnees.hydroquebec.com/api/explore/v2.1/catalog/datasets/importations-exportations-avec-transits/exports/csv?lang=fr&timezone=America%2FToronto&use_labels=true&delimiter=%2C"

    dictResultHQ = getJsonResponseResult(url=urlDownCSVHQ)

    fichier_a_envoyer = {
        'upload': open(dictResultHQ, 'rb'),
        }

    resourceIdToUpdate = '6ca279eb-3147-4a35-8a9e-e9f6fa4ac937' # retrouver votre id de ressource a modif
    # mettre les attributs a modifier
    resourceDictToModif = {
        'id' : resourceIdToUpdate,
        'description': 'description modifiée test',
    }
    
    response = requests.post(urlresourcePatch, data=resourceDictToModif,
                              files= fichier_a_envoyer,
                              headers=postHeader)
    
    if response.status_code == 200:
        print('Ressource MAJ avec succès.')
    else:
        print('Erreur lors de la modification de la ressource :', response.text)
        raise Exception(response.text)
    

    # Consulter la donnée du fichier CSV que nous venons de charger
    urlRequeteDatastoreSearch = urlActionPAB+"datastore_search?resource_id={}".format(resourceIdToUpdate)
    response = requests.get(urlRequeteDatastoreSearch,
                              headers=postHeader)
    json_data = json.loads(response.text)
    if response.status_code != 200:
        print('Erreur  :', response.text)
        raise Exception(response.text)

    reccords = json_data['result']['records']
    for unRec in reccords:
        print (unRec)
    print('fin')
