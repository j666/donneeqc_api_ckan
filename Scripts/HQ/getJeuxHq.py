import urllib3, json, sys
http = urllib3.PoolManager()

## Josée Martel 202502

import json, urllib, requests

##

def getJsonResponseResult(url, methode='GET'):
    # fonction générique pour des get à l'API HQ
    response = ""
    try:
        methode == 'GET'
        response = http.request('GET', url)
        reponseLoad = json.loads(response.data) # transforme la reponse en dictionnaire Python (json) 
        if response.status != 200:
            print('** Probleme avec la requete, cette requete est inexacte:\n {}'.format(url))
            raise Exception('Probleme avec la requete, cette requete est incorrecte:\n {}'.format(url))
        
        return reponseLoad

    except Exception as e:
        print('oh probleme', e)
        raise



if __name__ == '__main__':


    # charger le CSV a ajouter et/ou modifier 
    # fileRessource = "C:/Users/marjo31/local_job/test.csv"
    urlDownCSVHQ = "https://donnees.hydroquebec.com/api/explore/v2.1/catalog/datasets/importations-exportations-avec-transits/"

    reponseDeRequeteHQ = getJsonResponseResult(urlDownCSVHQ)

    for cle, valeur in reponseDeRequeteHQ.items():
        print(cle, valeur)


