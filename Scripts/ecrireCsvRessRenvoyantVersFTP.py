
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
# from services.serviceUtil import *


if __name__ == '__main__':

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']

    servUtil = ServicesUtils()
    objServiceApiDQ = ServiceAPI_DQ(jeton, enTest=False)

    packDict = objServiceApiDQ.getAllPackages()

    listDonneeAEcrire = []
    colCSV = [ 'pack_name', 'organization', 'author_email','id_res', 'res_name', 'url_res']
    listDonneeAEcrire.append(colCSV)

    for aPack in packDict.values():
        for resource in aPack['resources']:
            if resource['url'][0:3] == 'ftp' or resource['url'][0:3] == 'FTP' :
            # listInfoEcrire =
                listDonneeAEcrire.append([aPack['name'], aPack['organization']['title'],aPack['author_email'], resource['id'], resource['name'], resource['url']])


    fileCsvError = "C:/temp/{}_RessourceFTP.csv".format(objServiceApiDQ.getDateHeure_actuelle(heure=False))
    serCSV = ServiceCSV(fileCsvError)
    serCSV.ecrireCSVFromListOfList(listDonneeAEcrire)

    print('termine')