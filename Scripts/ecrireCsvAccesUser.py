
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *


if __name__ == '__main__':

    ### Script qui écrit 2 JSON et 2 CSV avec les utilisateurs et leur droit acces a DQ si admin ou editeur
    #####  attention des droits admin sont nécessaire pour lancer ce script et avoir toute les info comme le email.

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']
    objServDQ = ServiceAPI_DQ(jeton, enTest=False)

    ###########
    path = "C:/temp"

    #################
    # ATTENTION user doit avoir droit acces admin-edition
    ##############

    userList = objServDQ.getUserOrganizationEditor()
    dictOrgUserAdmin, dictOrgUserEditor = objServDQ.getOrganizationUserEditor(userList)

    # Écrire les 2 json
    objServDQ.writeJsonDict(dictOrgUserAdmin, path, 'orgUserAdmin')
    objServDQ.writeJsonDict(dictOrgUserEditor, path, 'orgUserEditor')

    #####
    ### CSV user = admin
    #########
    nomCSV = 'adminUserParOrganisation'
    pathNomCsv = '{0}/{1}_{2}.csv'.format(path, nomCSV, objServDQ.getDateHeure_actuelle(heure=False))
    listDonneeAEcrire = []
    colCSV = ['organisation', 'user_name', 'user_full_name', 'user_email' ]
    listDonneeAEcrire.append(colCSV)
    for org, listUser in dictOrgUserAdmin.items():
        for user in listUser:
            try:
                userEmail = user['email']
            except Exception as e:
                userEmail = 'Non disponible droit acces insuffisant'
            listDonneeAEcrire.append([org,user['name'],user['fullname'],userEmail])

    serCSV = ServiceCSV(pathNomCsv)
    serCSV.ecrireCSVFromListOfList(listDonneeAEcrire)

    #####
    ### CSV user = editor
    #########
    nomCSV = 'editorUserParOrganisation'
    pathNomCsv = '{0}/{1}_{2}.csv'.format(path, nomCSV, objServDQ.getDateHeure_actuelle(heure=False))
    listDonneeAEcrire = []
    colCSV = ['organisation', 'user_name', 'user_full_name', 'user_email']
    listDonneeAEcrire.append(colCSV)
    for org, listUser in dictOrgUserEditor.items():
        for user in listUser:
            try:
                userEmail = user['email']
            except Exception as e:
                userEmail = 'Non disponible droit acces insufisant'
            listDonneeAEcrire.append([org, user['name'],  user['fullname'], userEmail])

    serCSV.setFileCsv(pathNomCsv)
    serCSV.ecrireCSVFromListOfList(listDonneeAEcrire)

    print('ici')
