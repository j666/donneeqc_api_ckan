import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
from services.serviceUtil import *

from JETONSJOSEE import JETONSJOSEE
## importer un dict contenant les valeurs de vos jetons
# JETONSJOSEE = {
#     "PROD" : "xxxxx",
#     "BETA" : "xxyyyy"
# }


def ecrireCsvPackOrgDateCreationPlusPetitQueDate(servDQ, servCsv, org, dateACompare ):

    ### ecrire csv MRN ou date creation jeu < 2023-03-31

    packMrn = servDQ.getPackageListDictForAOrganization(organizationName=org)
    laDate = servDQ.servUtil.convertStringToDate(dateACompare)
    # laDate = servDQ.servUtil.convertStringToDate('2023-03-31')
    packTrueCondition = servDQ.filterDictPackCondition(listDictPack=packMrn, attribut='metadata_created', condition=laDate, operateur='<=', attributType='date')


    listOfListAttribut = servDQ.listOfListFromListOfDict(packTrueCondition, ['author', 'metadata_created', 'name', 'id'])
    listOfListAttribut.insert(0, ['Responsable', 'date_cree', 'nom_jeu', 'id_jeu'])

    servCsv.ecrireCSVFromListOfList(listOfListAttribut)
    print('Termine le csv a ete cree')



if __name__ == '__main__':
    # jeton = JETONSJOSEE['BETA']
    jeton = JETONSJOSEE['PROD']
    servDQ = ServiceAPI_DQ(jeton, enTest=False)
    # servCsv = ServiceCSV('C:/temp/mrnNew.csv')
    servCsv = ServiceCSV('C:/temp/test.csv')

    ecrireCsvPackOrgDateCreationPlusPetitQueDate(servDQ, servCsv, org='mrn', dateACompare='2017-01-31')






