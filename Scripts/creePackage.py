
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
from services.serviceUtil import *


if __name__ == '__main__':

    ### Script qui cree un plusieurs packages dans DQ a partir de fichier JSON d'un repertoire
    # package dans DQ

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']
    # jeton = JETONSJOSEE['BETA']
    # objServDQ = ServiceAPI_DQ(jeton, enTest=True)
    objServDQ = ServiceAPI_DQ(jeton, enTest=False)
    servUtil = ServicesUtils()
    # objServDQ.MAJ_deletePackage(idPackToDelete='candidatures', purge=True)

    # pathJsonACreer = "C:/Users/marjo31/local_job/temp/pack/pack_candidatures.json"
    # dictUnPack = objServDQ.getDictFromJsonRead(pathJsonACreer)
    # objServDQ.MAJ_createPackage(dictUnPack)

    listFileInDir = servUtil.getListPathAllFileInDir('C:/Users/marjo31/local_job/temp/pack/output_beta_20230509')
    for aFile in listFileInDir:
        dictUnPack = objServDQ.getDictFromJsonRead(aFile)
        dictUnPack['inventory_metadata'] = ['oui']
        objServDQ.MAJ_createPackage(dictUnPack)

    print('Le package a été crée')


