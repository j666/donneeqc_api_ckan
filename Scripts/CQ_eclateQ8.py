import json
import sys
# pour fonctionnement dans VS code, on doit ajouter le path pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")


from services.serviceCsv import *
from services.serviceConsultationQc import *
import copy


if __name__ == '__main__':

    jsonPath = "C:/Users/marjo31/local_job/temp/2024-53267.json"
    # jsonPath = "C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/consultationQc/20230720.json"
    servConsultation = ServiceConsultationQc(jsonPath)

    servConsultation.loadJson()

    newData = servConsultation.eclateReponseQuestionChoixMultiple(8)
    test = copy.deepcopy(newData)
    # test[1]["1. Comment avez-vous découvert l’existence du portail Données Québec?"] = 'fuck '
    newDataPlatenew = servConsultation.aplatirListReponse(test)
    listOfList = servConsultation.convertListDict2ListList(newDataPlatenew)

    pathCsvSortie = "C:/Users/marjo31/local_job/temp/cq_exportQuestion3Eclate.csv"
    servCsv = ServiceCSV(pathCsvSortie)
    servCsv.ecrireCSVFromListOfList(listOfList)