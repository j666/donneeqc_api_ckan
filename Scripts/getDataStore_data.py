

import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *



if __name__ == '__main__':

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['BETA']
    objServDQ = ServiceAPI_DQ(jeton, enTest=True)

    ###########
    idRessource = "c5c03a0a-06a4-4afe-87df-5c62bdaa3a60" #beta req entreprise 

    #################
    # ATTENTION user doit avoir droit acces admin-edition
    ##############
    result = objServDQ.getDatastore_search(idRessource)
    
    print('termine')