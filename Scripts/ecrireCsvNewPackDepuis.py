import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *


if __name__ == '__main__':
    # JM 2023-08
    ### Script qui écrit 1 CSV des nouveaux package depuis une date

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']
    # servDQ = ServiceAPI_DQ(jeton, enTest=False, environnement="MTL")
    servDQ = ServiceAPI_DQ(jeton, enTest=False, environnement="DQ")
    dateDebut = '2024-01-01'
    # dateDebut = '2024-06-01'
    allPackListDict = servDQ.getPackageListDictForDateCreation(dateBegin='2024-01-01', dateEnd='2024-12-31')
    # allPackListDict = servDQ.getPackageListDictForDateCreation(dateBegin=dateDebut, dateEnd='NOW')
    # allPackListDict = servDQ.getPackageListDictForDateCreation(dateBegin='2023-01-01', dateEnd='2023-06-01')

    fileCsvError = "C:/Users/marjo31/local_job/temp/{0}_packageCreated_depuis{1}.csv".format(servDQ.getDateHeure_actuelle(heure=False), dateDebut)

    servCSV = ServiceCSV(fileCsvError)

    servDQ.addOrganizationNameFirstLevelInListOfDictPack(allPackListDict)

    colToWrite = [ 'name', 'title', 'metadata_created', 'orgName' ]
    listOfListToWrite = servCSV.getListOfListToWrite(inputToWrite=allPackListDict, listColumn=colToWrite, inputType='listOfDict')
    # add organisation...

    listOfListToWrite.append(['total',len(allPackListDict)])
    servCSV.ecrireCSVFromListOfList(listOfListToWrite)

    print('terminé, csv des pack cree depuis sauvegardé')


