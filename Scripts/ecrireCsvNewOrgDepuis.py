import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")


from services.serviceAPI_DQ import *
from services.serviceCsv import *


if __name__ == '__main__':
    # JM 2023-08
    ### Script qui écrit 1 CSV des nouveaux package depuis une date

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']
    servDQ = ServiceAPI_DQ(jeton, enTest=False)

    orgListDict = servDQ.getOrganizationsDict()
    dateDebut = '2022-04-01'
    # dateDebut = '2020-01-01'

    listDictCreateUntilDate = servDQ.filterDictPackCondition(listDictPack=orgListDict, attribut='created', condition=dateDebut, operateur='>=', attributType='date')


    #ajouter la donnée de org principale au premier niv dans le dictOrg
    for aOrg in listDictCreateUntilDate:
        try:
            orgPrincipale = aOrg['groups'][0]['name']
            aOrg['orgParent'] = orgPrincipale
        except Exception as e:
            # pas d'organizsation principale donc en ai une elle meme
            aOrg['orgParent'] = aOrg['name']


    fileCsvError = "C:/Users/marjo31/local_job/temp/{0}_orgCreated_depuisDate_{1}.csv".format(servDQ.getDateHeure_actuelle(heure=False),dateDebut)
    servCSV = ServiceCSV(fileCsvError)

    colToWrite = [ 'display_name', 'id', 'created', 'package_count', 'orgParent']
    listOfListToWrite = servCSV.getListOfListToWrite(inputToWrite=listDictCreateUntilDate, listColumn=colToWrite, inputType='listOfDict')

    # # add organisation...
    #
    # listOfListToWrite.append(['total',len(allPackListDict)])
    servCSV.ecrireCSVFromListOfList(listOfListToWrite)

    print('terminé, csv des org cree depuis sauvegardé')


