import json
import sys
# pour fonctionnement dans VS code, on doit ajouter le path pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")


from services.serviceCsv import *
from services.serviceConsultationQc import *
from services.serviceUtil import *
import copy


if __name__ == '__main__':

    # pour le sondage de la consultation CQ sur DQ pour remplir le csv sur DQ pour les stat questions 3-6-11
    # 3 - Quelle raion viste DQ. choix multiple permit(telecharge, consulter, autre preciser:xxxx, etc)
    # 6 - Quel usage avez vous faites Données télécharge (un seul choix possible, devApp, journaliste, autre precise:xxx)
    # 11 - A quel titre utilisez vous DQ (un seul choix possible, devApp, autre preciser:xxxx)

    jsonPath = "C:/Users/marjo31/local_job/temp/cq/allTouteannee-CQ_DQ.json"
    # jsonPath = "C:/Users/marjo31/local_job/temp/2024-53267.json"
    # jsonPath = "C:/job/DonneeOuverte/DQ/GIT_Project/donneeTest/consultationQc/20230720.json"
    servConsultation = ServiceConsultationQc(jsonPath)
    servUtil = ServicesUtils()
    date = servUtil.getDateHeure_actuelle(heure=False)
    servConsultation.loadJson()

    servConsultation.jsonConserveUneAnne('2024')

    newData = servConsultation.eclateReponseQuestionChoixMultiple(3)

    test = copy.deepcopy(newData)

    newDataPlatenew = servConsultation.aplatirListReponse(test, questionExclure=3) # on est deja string donc on exclus
    listOfList = servConsultation.convertListDict2ListList(newDataPlatenew)


    pathCsvSortie = "C:/Users/marjo31/local_job/temp/cq/Q3_v4_2024_Eclate_{}.csv".format(date)
    
    servCsv = ServiceCSV(pathCsvSortie)
    servCsv.ecrireCSVFromListOfList(listOfList)
    print('fin')