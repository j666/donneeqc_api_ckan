
import sys
# pour fonctionnement dans VS code, on doit ajouter le path pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
from services.serviceTxt_new import *

import requests, os

### JM 2023
## script qui valide toutes les ressources CSV de DonnéesQC

if __name__ == '__main__':

    # servTxt = ServiceTxtNew('C:/temp/erreurValidationCsv.txt')
    # serCSV = ServiceCSV('C:/temp/erreurValidationCsv.csv')
    pathFichierErreur = "C:/Users/marjo31/local_job/temp/erreurValidationCsv.txt"
    servTxt = ServiceTxtNew(pathFichierErreur)
    serCSV = ServiceCSV('C:/Users/marjo31/local_job/temp/erreurValidationCsv.csv')

    servTxt.creer_fichier_ecrire_entete('Validation de tous les TYPE=CSV sur Donnée Québec')

    servApi = ServiceAPI_DQ(enTest=False)
    dictResCSV = servApi.getDictResourceTrueCondition(resourceAttribut='format', resourceAttributValue='CSV')
    print('nb ressource CSV:', len(dictResCSV))
    ## dowload file
    nbProb = 0
    listProb = []
    dicProb = {}
    servTxt.ecrire('ID,name,orgName,email,erreur,url_resource')
    listOfListToWrite = []
    listOfListToWrite.append(['ID','ResName','orgName','email','erreur','url_resource'])

    for id, dicRes in dictResCSV.items():

        # if id == '5bcabcca-61b4-487a-8971-52a5c1b1e974': #pas url...
        # if id == '1e341723-92cb-45e9-bdb5-220db5173e8c': #pas url...
        if id == 'f11b2ee7-f017-4df9-a3e7-2cab09b0039c': # prob zip...
            print('ici')      
        if id == '8db37749-6e61-472f-8da4-574c459e2a6f': # prob dilimiteur pas virgule...
            print('ici')  
        if id == '26b5e188-a8e7-457e-a1d4-04aea67870ae': # prob dilimiteur pas virgule...
            print('ici')      
        if id == '026ded58-51f6-4ac2-924e-a9ad4cf1c145': # prob ; separateur
            print('ici')      
        if id == 'b5453494-213c-41fb-90bf-d246b704d894': # prob | separateur
            print('ici')

        print('fichier no {}', dicRes['name'])
        url = dicRes['url']
        # if not url:
        #     print('ici')

        try:
            if url and url[-4:] == '.zip':
                raise ErreurApiDQ('Le fichier est compressé .zip')
            
            try:
                response = requests.get(url)
                if response.status_code == 403:
                    raise Exception('Réponse en problème 403 forbiden')
            except Exception as e:
                # continue
                if not url:
                    raise ErreurApiDQ('Problème URL est vide')
                raise ErreurApiDQ('Problème connection rencontré')
            
            url_content = response.content
            csv = open('temp.csv', 'wb')
            csv.write(url_content)
            csv.close()

            servCSV = ServiceCSV('temp.csv')
            servCSV.valideCsv()

        except Exception as e:
            print('Ressource CSV en probleme', e)
            # if isinstance(e, ErreurApiDQ):
            #     # cas ou nous n'avons pas le dic de la ressource
            #     servTxt.ecrire(stringToWrite)
            #     listOfListToWrite.append([dicRes['id'], strDecodeUTF_noBOM,org,email, e.__str__(), dicRes['url']])
            #     dicProb[dicRes['id']] = [strDecodeUTF_noBOM, dicRes['url'], e.__str__()]

            nbProb += 1
            listProb.append([dicRes['name'], id, dicRes['url']])
            dicProb[dicRes['id']] = [dicRes['name'], dicRes['url'], e.__str__()]
            packResourceProb = servApi.getPackForIdPack(dicRes['package_id'])
            org = packResourceProb['organization']['name']
            email = packResourceProb['author_email']
            stringToWrite = "{0},{1},{2},{3},{4},{5} ".format(dicRes['id'], dicRes['name'],org,email, e.__str__(),dicRes['url'] )
            try:

                servTxt.ecrire(stringToWrite)
                listOfListToWrite.append([dicRes['id'], dicRes['name'], org, email, e.__str__(), dicRes['url']])

            except Exception as erWrite:
                byteEncode = dicRes['name'].encode()
                strDecodeUTF_noBOM = byteEncode.decode('utf-8-sig') ## encodage sans le caractere BOM en debut de str
                stringToWrite = "{0},{1},{2},{3},{4},{5} ".format(dicRes['id'], strDecodeUTF_noBOM, org, email, e.__str__(), dicRes['url'])
                try:
                    servTxt.ecrire(stringToWrite)
                    listOfListToWrite.append([dicRes['id'], strDecodeUTF_noBOM,org,email, e.__str__(), dicRes['url']])
                    dicProb[dicRes['id']] = [strDecodeUTF_noBOM, dicRes['url'], e.__str__()]
                except Exception as e:
                    servTxt.ecrire('probleme encodage id_resource={}'.format(id))
                    listOfListToWrite.append([dicRes['id'], '',org,email, 'À verifier problème encodage bizarre', dicRes['url']])
                    pass # check pas normale arrive ici!!!

    serCSV.ecrireCSVFromListOfList(listOfListToWrite)
    os.remove('temp.csv')


        # reponse = requests., wb)
        # get(url, stream=True)
        # with open(reponse, newline="", encoding="utf-8") as csvfile:

        # with open('testCsv.csv', 'wb') as file:
        #     # A chunk of 128 bytes
        #     for chunk in response:
        #         file.write(chunk)

        # for line in response:
        #     print('ici')

        # reader = csv.reader(decoded_content , delimiter=',')
        # nbLigne = 0
        # for row in reader:
        #         nbLigne += 1
    servTxt.ecrire('\n\nNombre total de CSV validés: {0}.\n nb fichier erreur: {1}'.format(len(dictResCSV), nbProb))
    print('Validation completeDQ terminée!')
    # csvProb = 'ressourceProbleme.csv'
    # with open(csvProb, mode='w', encoding='utf-8') as csv_file:
    #     fieldNames = ['resource_name', 'resource_id' 'url']
    #     writer = csv.DictWriter(csv_file, fieldnames=fieldNames, delimiter=",")
    #     writer.writeheader()
    #     writer.writerow()