
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
# from services.serviceUtil import *

#JM 20250217 script qui ecrit un csv de toute les resource de DQ

if __name__ == '__main__':

    from JETONSJOSEE import JETONSJOSEE
    jeton = JETONSJOSEE['PROD']

    servUtil = ServicesUtils()
    objServiceApiDQ = ServiceAPI_DQ(jeton, enTest=False)

    packDict = objServiceApiDQ.getAllPackages()

    listDonneeAEcrire = []
    colCSV = [ 'organization', 'pack_name', 'pack_date_crea','res_id', 'res_name', 'res_date_crea']
    listDonneeAEcrire.append(colCSV)

    for aPack in packDict.values():
        for resource in aPack['resources']:

            # listInfoEcrire =
            listDonneeAEcrire.append([ aPack['organization']['title'],aPack['name'],aPack['metadata_created'], 
                                           resource['id'], resource['name'], resource['created']])


    fileCsvError = "C:/temp/{}_AllRessourceDQ.csv".format(objServiceApiDQ.getDateHeure_actuelle(heure=False))
    serCSV = ServiceCSV(fileCsvError)
    serCSV.ecrireCSVFromListOfList(listDonneeAEcrire)

    print('termine')