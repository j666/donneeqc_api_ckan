
import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionne ensuite
sys.path.append("../donneeqc_api_ckan")
from services.serviceCsv import *


if __name__ == '__main__':

    #### plusieurs CSV dans un repertoire ####

    # pathContenantCSVAValide = "C:/temp/aValideSODEC/"
    # pathContenantCSVAValide = "C:/Users/marjo31/local_job/temp/csv/"
    # # pathContenantCSVAValide = "C:/Users/marjo31/OneDrive - Gouv Qc/Documents/temp/recycl-quebec/"

    # servCSV = ServiceCSV(fileCsv='')
    # servCSV.validePlusieurCsv(pathContenantCSVAValide)


    ##### un CSV ###

    # pathCSV = "C:/Users/marjo31/local_job/temp/csv/LOISIR_PAYANT.csv"
    # pathCSV = "C:/Users/marjo31/local_job/temp/csv/SiteInvestigationGeo_BDG.csv"
    pathCSV = "C:/Users/marjo31/local_job/temp/csv/prob2/estimationprojectionuniquedonneesouvertes.csv"
    # pathCSV = "C:/Users/marjo31/local_job/temp/csv/frq/liste_financement_frqsc_2019-2020.csv"
    servCsv = ServiceCSV(fileCsv=pathCSV)
    servCsv.valideCsv()

    print('Validation terminé')



