import sys
# pour fonctionnement dans VS code, on doit ajouter le path du repertoire du dépot pour que l'import des packages fonctionnent
sys.path.append("../donneeqc_api_ckan")

from services.serviceAPI_DQ import *
from services.serviceCsv import *
from services.serviceUtil import *

from JETONSJOSEE import JETONSJOSEE
## importer un dict contenant les valeurs de vos jetons
# JETONSJOSEE = {
#     "PROD" : "xxxxx",
#     "BETA" : "xxyyyy"
# }


def ecrireCsvPackMAJDansLaSemaine(servDQ, servCsv):

    serUtil = ServicesUtils()
    date7DayAgo = serUtil.getDatePastNBDay(nbDayAgo=9) # set number of day


    allPack = servDQ.getAllPackages()
    listOfDictAllPack = servDQ.convertDictDict2ListDict(allPack)

    packTrueCondition = servDQ.filterDictPackCondition(listDictPack=listOfDictAllPack, attribut='metadata_modified', condition=date7DayAgo, operateur='>=', attributType='date')

    listOfListAttribut = servDQ.listOfListFromListOfDict(packTrueCondition, ['author', 'metadata_created',  'metadata_modified', 'name', 'id'])
    listOfListAttribut.insert(0, ['Responsable', 'date_cree', 'date_modif', 'nom_jeu', 'id_jeu'])

    servCsv.ecrireCSVFromListOfList(listOfListAttribut)
    print('Termine le csv a ete cree ici {}'.format(servCsv.fileCsv))


if __name__ == '__main__':
    # jeton = JETONSJOSEE['BETA']
    jeton = JETONSJOSEE['PROD']
    servDQ = ServiceAPI_DQ(jeton, enTest=False)
    # servCsv = ServiceCSV('C:/temp/mrnNew.csv')
    servCsv = ServiceCSV('C:/temp/export_pack_maj_derniere_semaine.csv')

    ecrireCsvPackMAJDansLaSemaine(servDQ, servCsv)








