# -*- coding: utf8 -*-
from __future__ import unicode_literals

from tkinter import Tk, Label, Button, Entry, N, S, E, W, IntVar, Radiobutton, END, HORIZONTAL, DISABLED, ACTIVE, Toplevel, RAISED, StringVar
from tkinter import filedialog as tkFileDialog
from tkinter import messagebox as tkMessageBox
import sys, os

import threading
from tkinter.ttk import Progressbar
from functools import partial
import time

from serviceAPI_DQ import *

import os
import time

class App_MAJ_ResourceDQ(Tk):
    # Application developpe pour ecrire un CSV incluant les id et info des ressources des fiches DQ
    # Josée Martel 2021-11

    def __init__(self):

        Tk.__init__(self)

        ## Grandeur et positionement de l'interface 
        w = 850
        h = 300
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
    
        x = (ws/2) - (w/2) # - 25
        y = ((hs/2) - (h/2)) - 250

        self.geometry('%dx%d+%d+%d' % (w, h, x, y))
        
        self.version = 1.0
        self.pathCSVFile = ''

        # Widget
        self.labelMessage = Label(self, text="Mise à jour des dates de modification de ressources pour les fiches DonnéeQC", fg="blue", font=("Helvetica", 11))
        self.labelMessage.grid(row=0,columnspan=2, padx=150, pady=10, sticky=W)

        self.labelMessage2 = Label(self, text="BIEN lire la documentation avant de lancer le traitement, aucun retour en arrière possible!", fg="red", font=("Helvetica", 11))
        self.labelMessage2.grid(row=1,columnspan=2, padx=120, sticky=W)

        self.labelCleApi = Label(self, text="Saisir votre clé API")
        self.labelCleApi.grid(row=2,column=0, padx=60, pady=30, sticky=W)

        self.boxCleApi = Entry(self, width=70)
        self.boxCleApi.grid(row=2, column=1, padx=15, pady=30, sticky=W)
        

        self.bt_pathCSV = Button(self, text="Indiquez le fichier CSV \n contenant les dates MAJ", command=partial(self.jopen, "fil", "boxPathCSV"), width=25)
        self.bt_pathCSV.grid(row=3, column=0, padx=10, pady=10)
        
        self.boxPathCSV = Entry(self, width=100)
        self.boxPathCSV.grid(row=3, column=1, padx=15, pady=10)

        self.bt_exe = Button(self, text="Exécuter la \n MAJ des dates", command=self.executer, fg="red", font="Verdana 10 bold", height=2, width=15,relief=RAISED)
        self.bt_fer = Button(self, text="Fermer", command=self.destroy, height=2, width=6, relief=RAISED )

        self.bt_exe.grid(row=4, column=1, sticky=W, padx=50, pady=10)
        self.bt_fer.grid(row=4, column=1, sticky=W, padx=230, pady=10)

        # self.progress = Progressbar(self, orient=HORIZONTAL, length=500, mode='determinate', value=0)
        # self.progress = Progressbar(self, orient=HORIZONTAL, length=200, mode='determinate', maximum=100, variable=self.prc_rendu, value=0)

        # attribut

        self.title("Modifier les dates de modification des ressources DQ. Version: {0}".format(self.version))


        ######### todo TEST a enlever
        # self.boxPathCSV.insert(0, 'C:/job/DonneeOuverte/DQ/test/leBon.csv')


    def jopen(self, option, nomBox):

        repNom = ""

        if option == "fil":
            repNom = tkFileDialog.askopenfilename(title="Pointer le fichier ", filetypes=[('CSV', '*.csv')])
        else:
            repNom = tkFileDialog.askdirectory(title="Indiquer le repertoire")

        leBox = getattr(self, nomBox)

        leBox.delete(0, END)
        leBox.insert(0, repNom)


    def checkPath(self):

        self.pathCSVFile = self.boxPathCSV.get()
        if not os.path.isfile(self.pathCSVFile ):
            raise ErreurApiDQ('Le chemin du fichier CSV indiqué est invalide')

        cleApi = self.boxCleApi.get()
        if not cleApi:
            raise ErreurApiDQ('Vous devez saisir une clé API pour effectuer des MAJ')
        
        self.serviceDQ = ServiceAPI_DQ(cleApi)
        return True


    def changeStateWidgetInterface(self, state):
        """methode qui change l'état des widget de l'interface 
        param state: 'normal', 'disabled'
        """

        self.bt_exe['state']= state
        self.bt_fer['state']= state
        self.boxCleApi['state']= state
        self.bt_pathCSV['state']= state
        self.boxPathCSV['state']= state



    def executer(self):
        try:         
            def real_traitement():


                try:
                    self.checkPath() 
                    self.changeStateWidgetInterface('disabled')
                    self.labelMessage['text'] = 'Traitement en cour, veuillez patienter...'
                    # self.progress['value'] = 2

                    #  get dict des changements 
                    dictResourceChange = self.serviceDQ.csvToDictResourceChangeIdNewVal(self.pathCSVFile, 'last_modified')
                    if len(dictResourceChange) == 0:
                            raise ErreurApiDQ('traitement interompu, aucun changement détecté dans le fichier CSV pour la colonne "new_last_modified"')
                    reponse = tkMessageBox.askquestion ('MAJ fiches DQ',"{0} modifications de l'attribut '{1}' seront réalisés. Veuillez vérifier que ce chiffre correspond aux modifications voulues.\nÊtes vous certain de vouloir lancer le traitement?".format(len(dictResourceChange), 'last_modified'),icon = 'warning')
                    if reponse == 'no':
                        raise ErreurApiDQ("traitement interrompu par l'utilisateur")
                    else:
                        self.serviceDQ.MAJ_resourceAttribut(dictResourceChange, 'last_modified', True)

                    # self.progress.stop()
                    # self.progress.grid_forget()

                    self.labelMessage['text'] = 'Traitement terminée avec succès! Les dates ont étées MAJ sur Donnée Québec'
                    
                    self.changeStateWidgetInterface('normal') 
                    print('fin')
                except Exception as e:
                    print(e)
                    if "main thread is not in main loop" in e.__str__():
                        tkMessageBox.showerror("Arret du traitement ecrireCSVInfoRessource", "Le traitement a été interrompu".format(self.repSortie))

                    elif type(e) == ErreurApiDQ:
                        tkMessageBox.showerror("Traitement interrompu", "{}".format(e.message))
                    else:
                        tkMessageBox.showerror("Traitement interrompu", "L'outil à rencontré un problème:\n {}, ".format(e))

                    self.changeStateWidgetInterface('normal')
                    self.labelMessage['text'] = "Un CSV resultera du traitement (voir la documentation pour les détails)"

                    # raise ## TODO a enlever pour test
            
            threading.Thread(target=real_traitement).start()

           
        except Exception as e:
            if "main thread is not in main loop" in e.__str__():
                tkMessageBox.showerror("Arret du traitement sortirCSVInfoRessource", "Le traitement a été interrompu".format(self.repSortie))
                self.removeCSV()
                return
              


if __name__ == '__main__':
    app = App_MAJ_ResourceDQ()
    app.mainloop()