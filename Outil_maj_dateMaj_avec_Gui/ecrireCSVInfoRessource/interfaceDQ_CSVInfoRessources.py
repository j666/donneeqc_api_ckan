# -*- coding: utf8 -*-
from __future__ import unicode_literals

from tkinter import Tk, Label, Button, Entry, N, S, E, W, IntVar, Radiobutton, END, HORIZONTAL, DISABLED, ACTIVE, Toplevel, RAISED, StringVar
from tkinter import filedialog as tkFileDialog
from tkinter import messagebox as tkMessageBox
import sys, os

import threading
# from tkinter.ttk import Progressbar
from functools import partial

from serviceAPI_DQ import *

import os


class App_getCSVInfoResourceDQ(Tk):
    # Application developpe pour ecrire un CSV incluant les id et info des ressources des fiches DQ
    # Josée Martel 2021-11

    def __init__(self):

        Tk.__init__(self)

        ## Grandeur et positionement de l'interface 
        w = 850
        h = 300
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
    
        x = (ws/2) - (w/2) # - 25
        y = ((hs/2) - (h/2)) - 250

        self.geometry('%dx%d+%d+%d' % (w, h, x, y))
        # self.geometry("850x300+600+200")

        self.version = 1.0

        self.encodage_fichier = StringVar()
        self.encodage_fichier.set("utf-8")
        self.serviceDQ = ServiceAPI_DQ()
        self.repSortie = ''
        self.CSVName = "infoRessourceDQ_{}.csv".format(self.serviceDQ.getDateHeure_actuelle())
        self.pathCSVFile = ''
        self.organisation = ''

        # def hide_me(event):
        #     print('hide')
        #     event.widget.pack_forget()

        # Widget
        self.labelMessage = Label(self, text="Un CSV resultera du traitement (voir la documentation pour les détails)", fg="blue", font=("Helvetica", 11))
        self.labelMessage.grid(row=0,column=1, padx=30, pady=20, sticky=W)

        self.labelMessageVide = Label(self, text=" ", fg="blue", font=("Helvetica", 11))
        self.labelMessageVide.grid(row=1,column=1, padx=100, sticky=W)

        self.labelOrg = Label(self, text="Organisation")
        self.labelOrg.grid(row=1,column=0, padx=50, pady=10, sticky=W)

        self.boxOrganisation = Entry(self, width=70)
        self.boxOrganisation.grid(row=1, column=1, padx=15, pady=10, sticky=W)
        self.boxOrganisation.insert(0, 'mffp')
        #

        self.bt_repSortie = Button(self, text="Répertoire de sortie du CSV", command=partial(self.jopen, "directorie", "boxRepSortie"), width=25)
        self.bt_repSortie.grid(row=2, column=0, padx=10, pady=10)
        #
        self.boxRepSortie = Entry(self, width=100)
        self.boxRepSortie.grid(row=2, column=1, padx=15, pady=10)


        self.labelEncodage = Label(self, text="Encodage fichier CSV")
        self.labelEncodage.grid(row=3,column=0, padx=30, pady=10, sticky=W)

        self.radio_utf8 = Radiobutton(self, text="utf-8", variable= self.encodage_fichier, value="utf-8")
        self.radio_1252 = Radiobutton(self, text="1252 (Pour utilisation avec Excel)", variable= self.encodage_fichier, value="1252")


        self.radio_utf8.grid(row=3, column=1,padx=40, pady=10, sticky=W)
        self.radio_1252.grid(row=3, column=1, padx=140, pady=10,  sticky=W)


        self.bt_exe = Button(self, text="Écrire CSV", command=self.executer, fg="red", font="Verdana 10 bold", height=2, width=9,relief=RAISED)
        self.bt_fer = Button(self, text="Fermer", command=self.destroy, height=2, width=6, relief=RAISED )

        self.bt_exe.grid(row=4, column=1, sticky=W, padx=80, pady=10)
        self.bt_fer.grid(row=4, column=1, sticky=W, padx=240, pady=10)


        # self.progress = Progressbar(self, orient=HORIZONTAL, length=500, mode='determinate', value=0)
        # self.progress = Progressbar(self, orient=HORIZONTAL, length=200, mode='determinate', maximum=100, variable=self.prc_rendu, value=0)


        # attribut

        self.title("Écrire CSV ressources DQ pour l'organisation. Version: {0}".format(self.version))


    def jopen(self, option, nomBox):

        repNom = ""
        if option == "fil":
            repNom = tkFileDialog.askopenfilename(title="Pointer le fichier ", filetypes=[('Geopackage', '*.gpkg')])
        else:
            repNom = tkFileDialog.askdirectory(title="Indiquer le repertoire")

        leBox = getattr(self, nomBox)

        leBox.delete(0, END)
        leBox.insert(0, repNom)


    def checkPath(self):

        self.repSortie = self.boxRepSortie.get()
        if not os.path.isdir(self.repSortie):
            tkMessageBox.showerror("Arret du traitement", "Le répertoire de sortie du CSV indiqué n'est pas valide")
            self.labelMessage['text'] = "Un CSV resultera du traitement (voir la documentation pour les détails)"
            return False

        self.pathCSVFile = self.repSortie+'/'+self.CSVName
        return True

    def removeCSV(self):
        if os.path.isfile(self.pathCSVFile):
            os.remove(self.pathCSVFile)

    def changeStateWidgetInterface(self, state):
        """methode qui change l'état des widget de l'interface 
        param state: 'normal', 'disabled'
        """

        self.bt_repSortie['state']= state
        self.boxRepSortie['state']= state
        self.bt_exe['state']= state
        self.bt_fer['state']= state
        self.boxOrganisation['state']= state
        self.radio_utf8['state']= state
        self.radio_1252['state']= state


    def executer(self):
        try:         
            def real_traitement():
                self.organisation = self.boxOrganisation.get()
                # self.progress.grid(row=1, column=1, padx=30, sticky=W)
                ok = self.checkPath()

                if ok:
                    try:
                        self.changeStateWidgetInterface('disabled')
                        self.labelMessage['text'] = 'Traitement en cour, veuillez patienter...'
                        # self.progress['value'] = 2

                        self.serviceDQ.writeCSVInfoResourceForOrganisation(self.organisation, self.pathCSVFile, self.encodage_fichier.get(), 'last_modified')

                        # self.progress.stop()
                        # self.progress.grid_forget()
                        if ok:
                            self.labelMessage['text'] = 'Traitement terminée avec succès! Le CSV est dans le repertoire de sortie'
                        self.changeStateWidgetInterface('normal') 

                    except Exception as e:
                        print(e)
                        if "main thread is not in main loop" in e.__str__():
                            tkMessageBox.showerror("Arret du traitement ecrireCSVInfoRessource", "Le traitement a été interrompu".format(self.repSortie))
                            self.removeCSV()
                        elif type(e) == ErreurApiDQ:
                            tkMessageBox.showerror("Problème rencontré", "{}".format(e.message))
                        else:
                            tkMessageBox.showerror("Problème", "L'outil à rencontré un problème:\n {}, ".format(e))
                            self.removeCSV()

                        self.changeStateWidgetInterface('normal')
                        self.labelMessage['text'] = "Un CSV resultera du traitement (voir la documentation pour les détails)"

                        # raise ## TODO a enlever pour test
            
            threading.Thread(target=real_traitement).start()

           
        except Exception as e:
            if "main thread is not in main loop" in e.__str__():
                tkMessageBox.showerror("Arret du traitement sortirCSVInfoRessource", "Le traitement a été interrompu".format(self.repSortie))
                self.removeCSV()
                return
              

if __name__ == '__main__':
    app = App_getCSVInfoResourceDQ()
    app.mainloop()